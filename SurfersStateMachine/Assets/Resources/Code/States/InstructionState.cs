﻿using UnityEngine;
using Assets.Resources.Code.Interfaces;
using Assets.Resources.Code.States;

namespace Assets.Resources.Code.States
{
    /// <summary>
    /// Instruction class.
    /// </summary>
    /// <remarks>show here the instruction for the game</remarks>
	class InstructionState: IStateBase
    {
        
		Camera myCamera;

		/// <summary>
        /// varaible to memorize the state manager object ( core of state machine )
        /// </summary>
        private StateManager manager;

        /// <summary>
        /// Create the state and load the Scene03
        /// </summary>
        /// <param name="managerRef"> store the state manager object reference</param>
        public InstructionState(StateManager managerRef)    // constructor
        {
            manager = managerRef;   // store a reference to StateManager class
			myCamera = Camera.main;
			myCamera.clearFlags = CameraClearFlags.Color;
            
        }

        /// <summary>
        /// Update is called by stateManager once per frame when state active
        /// </summary>
        public void StateUpdate()
        {
        }

        /// <summary>
        /// called by stateManager onGUI once per frame when state active
        /// </summary>
        public void ShowIt()
        {
			//sfondo
			GUI.DrawTexture(new Rect(0, 0, Screen.width,Screen.height), manager.gameDataRef.setting, ScaleMode.StretchToFill);


			// immagine
			GUI.DrawTexture(new Rect(Screen.width*1/2-200, 5, 400, 100), manager.gameDataRef.beginStateSplash, ScaleMode.ScaleToFit);
			
			GUIStyle labelStyle2 = new GUIStyle();
			labelStyle2.fontSize = 16;
			labelStyle2.normal.textColor = Color.white;
			labelStyle2.fontStyle = FontStyle.BoldAndItalic;
			labelStyle2.alignment = TextAnchor.MiddleLeft;
			labelStyle2.padding = new RectOffset (10, 0, 0, 0);
			
			GUIStyle labelStyle3 = new GUIStyle(labelStyle2);
			labelStyle3.fontSize = 14;
			//labelStyle3.normal.textColor = Color.white;
			labelStyle3.fontStyle = FontStyle.Normal;
			labelStyle3.alignment = TextAnchor.MiddleLeft;
			labelStyle3.padding = new RectOffset (10, 0, 0, 0);

			GUIStyle btStyleOFF = new GUIStyle(GUI.skin.button);
			btStyleOFF.normal.background = manager.gameDataRef.btOFF;
			btStyleOFF.fontSize = 14;
			btStyleOFF.normal.textColor = Color.white;
			btStyleOFF.fontStyle = FontStyle.Normal;
			btStyleOFF.alignment = TextAnchor.MiddleCenter;
			btStyleOFF.normal.textColor = Color.white;

			GUIStyle btStyleTUTORIAL = new GUIStyle (btStyleOFF);
			btStyleTUTORIAL.fixedWidth = 250;
			btStyleTUTORIAL.fixedHeight = 40;

			GUIStyle boxTra = new GUIStyle(GUI.skin.box);
			boxTra.normal.background = manager.gameDataRef.boxTra;
			boxTra.hover.background = manager.gameDataRef.boxTra;

			GUILayout.BeginArea (new Rect (10, Screen.height * 1 / 4, Screen.width-20, Screen.height * 3 / 4));
			GUILayout.BeginVertical ();
			GUILayout.Label ("MODALITA' SALTA",labelStyle2);
			GUILayout.Space (10);
			GUILayout.Label (	"Dopo aver memorizzato il percorso, il giocatore deve trascinare il personaggio seguendo l'ordine corretto.\n",labelStyle3);
			GUILayout.Label (	 "Arrivato in prossimità dell'ostacolo l'utente dovrà esercitare una forza in un range richiesto per un certo\n",labelStyle3);
			GUILayout.Label (	 "periodo di tempo e questo permetterà l'esecuzione del salto.\n",labelStyle3);
			GUILayout.Label (	 "Una volta superati tutti gli ostacoli il giocatore dovrà portarsi sul bollino rosso che contraddistingue l'arrivo.",labelStyle3);
			// if button pressed or T keys
			GUILayout.Space (10);
			if (GUILayout.Button("Tutorial",btStyleTUTORIAL) || Input.GetKeyUp(KeyCode.T) ) {
				// set permanence timer for the force thereshold to a value
				manager.gameDataRef.forceManager.RemainingTimeSetted = 5;
				
				// set lowr limt for the force
				manager.gameDataRef.forceManager.ForceLowerLimit = 5;	// switch the state to Setup state
				manager.SwitchState(new TutorialState(manager));
			}

			GUILayout.Space (20);
			GUILayout.Label ("ACCHIAPPA",labelStyle2);
			GUILayout.Space (10);
			GUILayout.Label (	"In questa modalità l'ordine degli ostacoli è definito dalla sequenza di operazioni caratteristiche di un azione\n",labelStyle3);
			GUILayout.Label (	"quotidiana, in questo caso la preparazione di un caffè.\n",labelStyle3);
			GUILayout.Label (	"La raccolta degli oggetti sarà scandita dal comporsi di una moka in un angolo dello schermo.\n",labelStyle3);
			GUILayout.EndVertical ();
			GUILayout.EndArea ();



            // if button pressed or R keys
			if (GUI.Button(new Rect(Screen.width - 250 - 5, 5, 250, 60), "Torna al menù",btStyleOFF) || Input.GetKeyUp(KeyCode.R) )
            {
                // switch the state to Setup state
                manager.SwitchState(new SetupState(manager));
            }
            Debug.Log("In InstructionState");
        }
    }
}