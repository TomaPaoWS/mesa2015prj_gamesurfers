﻿using UnityEngine;
using Assets.Resources.Code.Interfaces;

namespace Assets.Resources.Code.States
{
    /// <summary>
    /// Initial State. Usually is the initial screenshot
    /// </summary>
    public class BeginState : IStateBase {
        // varaible to memorize the state manager object (core of state machine)
        private StateManager manager;
		bool premuto = false;
		GUIStyle uno,due;

        // Create the state and load the Scene00
        // <param name="managerRef"> store the state manager object reference</param>
        public BeginState(StateManager managerRef) {
            manager = managerRef;   // store a reference to StateManager class

            // Load Scene00
            if (Application.loadedLevelName != "Scene00") {
                Application.LoadLevel("Scene00");
			}

        }

        // Update is called by stateManager once per frame when state active
        public void StateUpdate() {
        }

        // called by stateManager onGUI once per frame when state active
        public void ShowIt() {

			// immagine
			GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), manager.gameDataRef.title, ScaleMode.ScaleAndCrop);


			GUIStyle btStyleOFF = new GUIStyle(GUI.skin.button);
			btStyleOFF.normal.background = manager.gameDataRef.btOFF;
			btStyleOFF.fontSize = 14;
			btStyleOFF.normal.textColor = Color.white;
			btStyleOFF.fontStyle = FontStyle.Normal;
			btStyleOFF.alignment = TextAnchor.MiddleCenter;
			btStyleOFF.normal.textColor = Color.white;
			GUIStyle btStyleON = new GUIStyle(btStyleOFF);
			btStyleON.normal.background = manager.gameDataRef.btON;
			btStyleON.hover.background = manager.gameDataRef.btON;


			if (GUI.Button(new Rect(10, 5, 250, 60), "Entra!",btStyleOFF) || Input.GetKeyUp(KeyCode.S) ) {
				// switch the state to Setup state
				manager.SwitchState(new SetupState(manager));
			}





            Debug.Log("In BeginState");
        }
    }
}