﻿using UnityEngine;
using System.IO;
using Assets.Resources.Code.Interfaces;
using Assets.Resources.Code.States;

namespace Assets.Resources.Code.States  {
    // Setup Class. use this class to set the option value
    class SetupState : IStateBase {
        // varaible to memorize the state manager object ( core of state machine )
        private StateManager manager;
		AudioSource music; 
		AudioClip canzone;

		// parametri***************************************
		public float tr_min;
		public float tr_max;
		public float s1_min;
		public float s1_max;
		public float s2_min;
		public float s2_max;
		public float ostacoliSel;
		public float tempoSel;
		
		public bool tipoGioco;	//true = salta!!! false = acchiappa!!!
		public bool abilitaLinee;
		public bool disabilitaTrascinamento;
		public bool disabilitaTurbinio;
		public bool ostacoliDiversi;
		public bool sensForza;
		public bool volume;
		//*************************************************

		float[] parFloat;
		bool[] parBool;

		Camera myCamera;

		// Create the state and load the Scene00
        public SetupState(StateManager managerRef) {   // constructor
            manager = managerRef;   // store a reference to StateManager class
			myCamera = Camera.main;
			myCamera.clearFlags = CameraClearFlags.Color;

            // Load Scene00
            if (Application.loadedLevelName != "Scene00") {
                Application.LoadLevel("Scene00");
            }

			parFloat = manager.gameDataRef.parFloat;
			parBool = manager.gameDataRef.parBool;

			tr_min = parFloat[0];
			tr_max = parFloat[1];
			s1_min = parFloat[2];
			s1_max = parFloat[3];
			s2_min = parFloat[4];
			s2_max = parFloat[5];
			ostacoliSel = parFloat[6];
			tempoSel = parFloat[7];

			tipoGioco = parBool[0];	//true = salta!!! false = acchiappa!!!
			abilitaLinee = parBool[1];
			disabilitaTrascinamento = parBool[2];
			disabilitaTurbinio = parBool[3];
			ostacoliDiversi = parBool [4];
			sensForza = parBool[5];
			volume = parBool[6];


			music = myCamera.GetComponent<AudioSource> ();
			canzone = GameObject.Instantiate (UnityEngine.Resources.Load ("musicaMenu")) as AudioClip;
			music.clip = canzone;


		


		}


        // Update is called by stateManager once per frame when state active
        public void StateUpdate() {
			if (volume) {
				music.Stop ();
			} else if (!music.isPlaying){
				music.Play ();
			}
        }







		GUIStyle uno,due,tre,quattro,cinque,sei;
		float hBt = 40f; // altrezza minima pulsanti
		float spaceBt = 20f; // distanza tra pulsanti
		float delta = Screen.height*0.5f;


        // called by stateManager onGUI once per frame when state active
        public void ShowIt() {
			bool aiuto0 = false;
			bool aiuto1 = false;
			bool aiuto2 = false;
			bool aiuto3 = false;
			bool aiuto4 = false;
			bool aiuto5 = false;

			//sfondo
			GUI.DrawTexture(new Rect(0, 0, Screen.width,Screen.height), manager.gameDataRef.setting, ScaleMode.StretchToFill);


			// immagine
			GUI.DrawTexture(new Rect(Screen.width*1/2-200, 5, 400, 100), manager.gameDataRef.beginStateSplash, ScaleMode.ScaleToFit);

			GUIStyle labelStyle2 = new GUIStyle();
			labelStyle2.fontSize = 16;
			labelStyle2.normal.textColor = Color.white;
			labelStyle2.fontStyle = FontStyle.BoldAndItalic;
			labelStyle2.alignment = TextAnchor.MiddleLeft;
			labelStyle2.padding = new RectOffset (10, 0, 0, 0);

			GUIStyle labelStyle3 = new GUIStyle(labelStyle2);
			labelStyle3.fontSize = 14;
			//labelStyle3.normal.textColor = Color.white;
			labelStyle3.fontStyle = FontStyle.Normal;
			labelStyle3.alignment = TextAnchor.MiddleLeft;
			labelStyle3.padding = new RectOffset (10, 0, 0, 0);

			GUIStyle labelStyle5 = new GUIStyle(labelStyle3);
			labelStyle5.wordWrap = true;

			//GUI.skin.button.normal.background = manager.gameDataRef.btOFF;
			GUIStyle btStyleOFF = new GUIStyle(GUI.skin.button);
			btStyleOFF.normal.background = manager.gameDataRef.btOFF;
			btStyleOFF.fontSize = 14;
			btStyleOFF.normal.textColor = Color.white;
			btStyleOFF.fontStyle = FontStyle.Normal;
			btStyleOFF.alignment = TextAnchor.MiddleCenter;
			btStyleOFF.normal.textColor = Color.white;
			GUIStyle btStyleON = new GUIStyle(btStyleOFF);
			btStyleON.normal.background = manager.gameDataRef.btON;
			btStyleON.hover.background = manager.gameDataRef.btON;

			GUIStyle boxStyle = new GUIStyle(GUI.skin.box);
			boxStyle.normal.background = manager.gameDataRef.boxForza;
			boxStyle.hover.background = manager.gameDataRef.boxForza;

			GUIStyle boxTra = new GUIStyle(GUI.skin.box);
			boxTra.normal.background = manager.gameDataRef.boxTra;
			boxTra.hover.background = manager.gameDataRef.boxTra;

			GUIStyle boxInfo = new GUIStyle(GUI.skin.box);
			boxInfo.normal.background =  manager.gameDataRef.nero;
			boxInfo.hover.background = manager.gameDataRef.nero;
			boxInfo.fontStyle = FontStyle.Normal;
			boxInfo.alignment = TextAnchor.MiddleLeft;
			boxInfo.fontSize = 14;

			GUIStyle boxInfoImg = new GUIStyle(boxTra);
			boxInfoImg.fixedHeight = 20;
			boxInfoImg.fixedWidth = 25;
			boxInfoImg.overflow = new RectOffset (0, 0, 0, 0);
			boxInfoImg.border = new RectOffset (0, 0, 0, 0);



			GUIStyle boxToggle = new GUIStyle(GUI.skin.toggle);
			boxToggle.imagePosition = ImagePosition.ImageOnly;
			boxToggle.overflow = new RectOffset (0, 0, 0, 0);
			boxToggle.border = new RectOffset (0, 0, 0, 0);
			boxToggle.fixedWidth = 30f;
			boxToggle.fixedHeight = 30f;
			GUIStyle labelStyle4 = new GUIStyle(labelStyle3);
			labelStyle4.fixedHeight = 30f;

			// zona di sinistra
			GUILayout.BeginArea(new Rect(1, Screen.height*1/6, Screen.width*1/2-50, Screen.height*5/6));
			GUILayout.BeginVertical(boxTra); // tutti i settori

			// tipo di gioco-------------------------------------------------------------------------
			GUILayout.BeginVertical(boxTra); // singola domanda
			GUILayout.Label("Selezionare il tipo di Gioco:",labelStyle2);
			GUILayout.BeginHorizontal(boxTra); // risposte
			//GUILayout.MinHeight(100);
			if (tipoGioco) { //è selezionato salta
				uno = btStyleON;
				due = btStyleOFF;
			} else { //è selezionato acchiappa
				uno = btStyleOFF;
				due = btStyleON; }
			if (GUILayout.Button ("Salta", uno,GUILayout.MinHeight(hBt))) {tipoGioco = true; }
			GUILayout.Space (spaceBt);
			if (GUILayout.Button ("Acchiappa", due,GUILayout.MinHeight(hBt))) {tipoGioco = false; }
			GUILayout.EndHorizontal(); // risposte
			GUILayout.EndVertical ();

			// numero di ostacoli---------------------------------------------------------------------
			if (tipoGioco) {
				GUILayout.BeginVertical (boxTra); // singola domanda
				GUILayout.Label ("Selezionare il numero di ostacoli:", labelStyle2);
				GUILayout.BeginHorizontal (boxTra); // risposte
				switch (Mathf.RoundToInt (ostacoliSel)) {
				case 2:
					due = btStyleON;
					tre = quattro = cinque = sei = btStyleOFF;
					break;
				case 3:
					tre = btStyleON;
					due = quattro = cinque = sei = btStyleOFF;
					break;
				case 4:
					quattro = btStyleON;
					due = tre = cinque = sei = btStyleOFF;
					break;
				case 5:
					cinque = btStyleON;
					due = tre = quattro = sei = btStyleOFF;
					break;
				case 6:
					sei = btStyleON;
					due = tre = quattro = cinque = btStyleOFF;
					break;
				}
				if (GUILayout.Button ("2", due, GUILayout.MinHeight (hBt))) {
					ostacoliSel = 2f;
				}
				GUILayout.Space (spaceBt);
				if (GUILayout.Button ("3", tre, GUILayout.MinHeight (hBt))) {
					ostacoliSel = 3f;
				}
				GUILayout.Space (spaceBt);
				if (GUILayout.Button ("4", quattro, GUILayout.MinHeight (hBt))) {
					ostacoliSel = 4f;
				}
				GUILayout.Space (spaceBt);
				if (GUILayout.Button ("5", cinque, GUILayout.MinHeight (hBt))) {
					ostacoliSel = 5f;
				}
				GUILayout.Space (spaceBt);
				if (GUILayout.Button ("6", sei, GUILayout.MinHeight (hBt))) {
					ostacoliSel = 6f;
				}
				
				GUILayout.EndHorizontal (); // risposte
				GUILayout.EndVertical (); //domanda
			}

			// tempo tra gli ostacoli---------------------------------------------------------------------
			if (tipoGioco) {
				GUILayout.BeginVertical (boxTra); // singola domanda
				GUILayout.BeginHorizontal(boxTra);
				GUILayout.Label ("Selezionare il tempo tra gli ostacoli:", labelStyle2);
				GUILayout.Box(manager.gameDataRef.info,boxInfoImg);
				if(Event.current.type == EventType.Repaint && GUILayoutUtility.GetLastRect().Contains(Event.current.mousePosition )){
					aiuto0 =true;
				}
				GUILayout.FlexibleSpace();
				GUILayout.EndHorizontal ();

				GUILayout.BeginHorizontal (boxTra); // risposte
				switch (Mathf.RoundToInt (tempoSel)) {
				case 2:
					due = btStyleON;
					tre = quattro = cinque = btStyleOFF;
					break;
				case 3:
					tre = btStyleON;
					due = quattro = cinque = btStyleOFF;
					break;
				case 4:
					quattro = btStyleON;
					due = tre = cinque = btStyleOFF;
					break;
				case 0:
					cinque = btStyleON;
					due = tre = quattro = btStyleOFF;
					break;
				}
				if (GUILayout.Button ("2", due, GUILayout.MinHeight (hBt))) {
					tempoSel = 2f;
				}
				GUILayout.Space (spaceBt);
				if (GUILayout.Button ("3", tre, GUILayout.MinHeight (hBt))) {
					tempoSel = 3f;
				}
				GUILayout.Space (spaceBt);
				if (GUILayout.Button ("4", quattro, GUILayout.MinHeight (hBt))) {
					tempoSel = 4f;
				}
				GUILayout.Space (spaceBt);
				if (GUILayout.Button ("variabile", cinque, GUILayout.MinHeight (hBt))) {
					tempoSel = 0f;
				}

				GUILayout.EndHorizontal (); // risposte
				GUILayout.EndVertical (); // domanda

			} else { //acchiappa
				GUILayout.BeginHorizontal (boxTra); // caffe
				GUILayout.Toggle(true, "",boxToggle);
				GUILayout.Label ("Fare il caffè",labelStyle4);
				GUILayout.EndHorizontal ();

				GUILayout.BeginHorizontal (boxTra); // altri
				GUILayout.Toggle(false, "",boxToggle);
				GUILayout.Label ("---",labelStyle4);
				GUILayout.EndHorizontal ();
			}



			// aiuti---------------------------------------------------------------------------------
			GUILayout.BeginVertical (boxTra); // singola domanda
			GUILayout.Label ("Selezionare gli aiuti:",labelStyle2);
			if (tipoGioco) {//aiuti per salta
				GUILayout.BeginHorizontal (boxTra); // risposte
				abilitaLinee = GUILayout.Toggle (abilitaLinee, "", boxToggle);
				GUILayout.Label ("Visualizza percorso", labelStyle4);
				GUILayout.Box(manager.gameDataRef.info,boxInfoImg);
				if(Event.current.type == EventType.Repaint && GUILayoutUtility.GetLastRect().Contains(Event.current.mousePosition )){
					aiuto1 =true;
				}
				GUILayout.FlexibleSpace();
				GUILayout.EndHorizontal ();

				GUILayout.BeginHorizontal (boxTra); // risposte
				disabilitaTrascinamento = GUILayout.Toggle (disabilitaTrascinamento, "", boxToggle);
				GUILayout.Label ("Disabilita trascinamento", labelStyle4);
				GUILayout.Box(manager.gameDataRef.info,boxInfoImg);
				if(Event.current.type == EventType.Repaint && GUILayoutUtility.GetLastRect().Contains(Event.current.mousePosition )){
					aiuto2 =true;
				}
				GUILayout.FlexibleSpace();
				GUILayout.EndHorizontal ();

				GUILayout.BeginHorizontal (boxTra); // risposte
				ostacoliDiversi = GUILayout.Toggle (ostacoliDiversi, "", boxToggle);
				GUILayout.Label ("Ostacoli diversi", labelStyle4);
				GUILayout.Box(manager.gameDataRef.info,boxInfoImg);
				if(Event.current.type == EventType.Repaint && GUILayoutUtility.GetLastRect().Contains(Event.current.mousePosition )){
					aiuto3 =true;
				}
				GUILayout.FlexibleSpace();
				GUILayout.EndHorizontal ();

				GUILayout.BeginHorizontal (boxTra); // risposte
				disabilitaTurbinio = GUILayout.Toggle (disabilitaTurbinio, "", boxToggle);
				GUILayout.Label ("Mantieni ostacoli", labelStyle4);
				GUILayout.Box(manager.gameDataRef.info,boxInfoImg);
				if(Event.current.type == EventType.Repaint && GUILayoutUtility.GetLastRect().Contains(Event.current.mousePosition )){
					aiuto4 =true;
				}
				GUILayout.FlexibleSpace();
				GUILayout.EndHorizontal ();
			
			} else {//aiuti per acchiappa
				GUILayout.BeginHorizontal (boxTra); // risposte
				disabilitaTrascinamento = GUILayout.Toggle (disabilitaTrascinamento, "", boxToggle);
				GUILayout.Label ("Disabilita trascinamento", labelStyle4);
				GUILayout.Box(manager.gameDataRef.info,boxInfoImg);
				if(Event.current.type == EventType.Repaint && GUILayoutUtility.GetLastRect().Contains(Event.current.mousePosition )){
					aiuto2 =true;
				}
				GUILayout.FlexibleSpace();
				GUILayout.EndHorizontal ();

			}

			GUILayout.BeginHorizontal (boxTra); // risposte
			volume = GUILayout.Toggle (volume, "", boxToggle);
			GUILayout.Label ("Disabilita suoni", labelStyle4);
			GUILayout.EndHorizontal ();
			
			GUILayout.EndVertical (); // aiuti

			GUILayout.EndVertical ();
			GUILayout.EndArea ();	//zona di sinistra


			// zona di destra
			GUILayout.BeginArea(new Rect(Screen.width*1/2, Screen.height*1/6, Screen.width*1/2-10, Screen.height*5/6));
			GUILayout.BeginVertical(boxTra); // tutti i settori

			// sensore di forza-------------------------------------------------------------------------
			GUILayout.BeginVertical(boxTra); // singola domanda
			GUILayout.BeginHorizontal (boxTra);
			GUILayout.Label("Gestione forza:",labelStyle2);
			GUILayout.Box(manager.gameDataRef.info,boxInfoImg);
			if(Event.current.type == EventType.Repaint && GUILayoutUtility.GetLastRect().Contains(Event.current.mousePosition )){
				aiuto5 =true;
			}
			GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal ();

			GUILayout.BeginHorizontal(boxTra); // risposte
			if (sensForza) { //è selezionato salta
				uno = btStyleON;
				due = btStyleOFF;
			} else { //è selezionato acchiappa
				uno = btStyleOFF;
				due = btStyleON; }
			if (GUILayout.Button ("Si", uno,GUILayout.MinHeight(hBt))) {sensForza = true; }
			GUILayout.Space (spaceBt);
			if (GUILayout.Button ("No", due,GUILayout.MinHeight(hBt))) {sensForza = false; }
			GUILayout.EndHorizontal(); // risposte
			GUILayout.EndVertical ();


			// barra di forza
			delta = Screen.height*0.45f;
			if (sensForza) {
				GUILayout.BeginVertical(boxTra); //singola domanda
				GUILayout.BeginHorizontal(boxTra);
				/*
				GUILayout.BeginVertical(boxTra,GUILayout.MaxWidth(20)); //barra di forza
				GUILayout.Label("Barra\nforza",labelStyle3);
				GUILayout.Space (10);
				GUILayout.Box ("", boxStyle,GUILayout.Height(delta),GUILayout.MaxWidth(20));
				GUILayout.EndVertical ();*/

				GUILayout.BeginVertical(boxTra,GUILayout.MaxWidth(15)); // trascinamento
				GUILayout.Label("Range\ntrascinamento",labelStyle3);
				GUILayout.BeginHorizontal(boxTra);
				GUILayout.BeginVertical(boxTra);
				GUILayout.Space (2);
				GUILayout.Box ("", boxStyle,GUILayout.Height(delta),GUILayout.MaxWidth(5));
				GUILayout.EndVertical();
				GUILayout.BeginVertical(boxTra);
				tr_max = GUILayout.VerticalSlider( tr_max, 1, (tr_min+tr_max)/2,GUILayout.Height(delta*(1-(tr_min+tr_max)/2)));
				tr_min = GUILayout.VerticalSlider( tr_min, (tr_min+tr_max)/2, 0,GUILayout.Height(delta*((tr_min+tr_max)/2)));
				GUILayout.EndVertical();
				GUILayout.BeginVertical(boxTra);
				GUILayout.Space (delta*(1-tr_max)-10);
				GUILayout.Label (Mathf.Round (tr_max * 100) + " %");
				GUILayout.Space (delta*(tr_max-tr_min)-20);
				GUILayout.Label (Mathf.Round (tr_min * 100) + " %");
				GUILayout.EndVertical();
				GUILayout.EndHorizontal();
				GUILayout.EndVertical ();

				GUILayout.BeginVertical(boxTra,GUILayout.MaxWidth(15)); // soglia1
				if(tipoGioco){
					GUILayout.Label("Range salto\npiccolo",labelStyle3);
				} else{
					GUILayout.Label("Range\nafferraggio",labelStyle3);
				}
				GUILayout.BeginHorizontal(boxTra);
				GUILayout.BeginVertical(boxTra);
				GUILayout.Space (2);
				GUILayout.Box ("", boxStyle,GUILayout.Height(delta),GUILayout.MaxWidth(5));
				GUILayout.EndVertical();
				GUILayout.BeginVertical(boxTra);
				s1_max = GUILayout.VerticalSlider( s1_max, 1, (s1_min+s1_max)/2,GUILayout.Height(delta*(1-(s1_min+s1_max)/2)));
				s1_min = GUILayout.VerticalSlider( s1_min, (s1_min+s1_max)/2, 0,GUILayout.Height(delta*((s1_min+s1_max)/2)));
				GUILayout.EndVertical();
				GUILayout.BeginVertical(boxTra);
				GUILayout.Space (delta*(1-s1_max)-10);
				GUILayout.Label (Mathf.Round (s1_max * 100) + " %");
				GUILayout.Space (delta*(s1_max-s1_min)-20);
				GUILayout.Label (Mathf.Round (s1_min * 100) + " %");
				GUILayout.EndVertical();
				GUILayout.EndHorizontal();
				GUILayout.EndVertical ();

				if(tipoGioco){
					GUILayout.BeginVertical(boxTra,GUILayout.MaxWidth(15)); // soglia2
					GUILayout.Label("Range salto\ngrande",labelStyle3);
					GUILayout.BeginHorizontal(boxTra);
					GUILayout.BeginVertical(boxTra);
					GUILayout.Space (2);
					GUILayout.Box ("", boxStyle,GUILayout.Height(delta),GUILayout.MaxWidth(5));
					GUILayout.EndVertical();
					GUILayout.BeginVertical(boxTra);
					s2_max = GUILayout.VerticalSlider( s2_max, 1, (s2_min+s2_max)/2,GUILayout.Height(delta*(1-(s2_min+s2_max)/2)));
					s2_min = GUILayout.VerticalSlider( s2_min, (s2_min+s2_max)/2, 0,GUILayout.Height(delta*((s2_min+s2_max)/2)));
					GUILayout.EndVertical();
					GUILayout.BeginVertical(boxTra);
					GUILayout.Space (delta*(1-s2_max)-10);
					GUILayout.Label (Mathf.Round (s2_max * 100) + " %");
					GUILayout.Space (delta*(s2_max-s2_min)-20);
					GUILayout.Label (Mathf.Round (s2_min * 100) + " %");
					GUILayout.EndVertical();
					GUILayout.EndHorizontal();
					GUILayout.EndVertical ();
				}

				GUILayout.EndHorizontal();
				GUILayout.EndVertical ();

			} else {
				tr_max = 1f;
				tr_min = 0f;
				s1_max = 1f;
				s1_min = 0f;
				s2_max = 1f;
				s2_min = 0f;
			}


			manager.gameDataRef.parFloat[0] = tr_min;
			manager.gameDataRef.parFloat[1] = tr_max;
			manager.gameDataRef.parFloat[2] = s1_min;
			manager.gameDataRef.parFloat[3] = s1_max;
			manager.gameDataRef.parFloat[4] = s2_min;
			manager.gameDataRef.parFloat[5] = s2_max;
			manager.gameDataRef.parFloat[6] = ostacoliSel;
			manager.gameDataRef.parFloat[7] = tempoSel;

			manager.gameDataRef.parBool[0] =tipoGioco ;
			manager.gameDataRef.parBool[1] =abilitaLinee ;
			manager.gameDataRef.parBool[2] =disabilitaTrascinamento ;
			manager.gameDataRef.parBool[3] =disabilitaTurbinio ;
			manager.gameDataRef.parBool[4] =ostacoliDiversi ;
			manager.gameDataRef.parBool[5] =sensForza ;
			manager.gameDataRef.parBool[6] =volume ;






			/*
			// barra di forza
			delta = Screen.height/2-40;
			if (sensForza) {
				GUILayout.BeginArea (new Rect (Screen.width/2-20, Screen.height*1/2, Screen.width / 2, delta + 40));
				GUILayout.Label (new Rect (0, 0, 60, 20), "Impostazioni barra di forza", labelStyle2);
				GUILayout.Box (new Rect (60, 30, 20, delta), "", boxStyle);
			
				max = GUILayout.VerticalSlider (new Rect (90, 30, 20, delta * (1 - soglia2)), max, 1, soglia2);
				soglia2 = GUILayout.VerticalSlider (new Rect (120, 30 + delta * (1 - max), 20, delta * (max - soglia1)), soglia2, max, soglia1);
				soglia1 = GUILayout.VerticalSlider (new Rect (90, 30 + delta * (1 - soglia2), 20, delta * (soglia2 - min)), soglia1, soglia2, min);
				min = GUILayout.VerticalSlider (new Rect (120, 30 + delta * (1 - soglia1), 20, delta * (soglia1 - 0f)), min, soglia1, 0);

				GUILayout.Box (new Rect (20, 25 + delta * (1 - max), 65, 10), "",GUI.skin.box);
				GUILayout.Box (new Rect (20, 25 + delta * (1 - soglia2), 95, 10), "",GUI.skin.box);
				GUILayout.Box (new Rect (20, 25 + delta * (1 - soglia1), 65, 10), "",GUI.skin.box);
				GUILayout.Box (new Rect (20, 25 + delta * (1 - min), 95, 10), "",GUI.skin.box);

				GUILayout.Label (new Rect (140, 20 + delta * (1 - max), 100, 20), "Massimo", labelStyle3);
				GUILayout.Label (new Rect (140, 20 + delta * (1 - soglia2), 100, 20), "Soglia 2", labelStyle3);
				GUILayout.Label (new Rect (140, 20 + delta * (1 - soglia1), 100, 20), "Soglia 1", labelStyle3);
				GUILayout.Label (new Rect (140, 20 + delta * (1 - min), 100, 20), "Minimo", labelStyle3);

				GUILayout.Label (new Rect (21, 20 + delta * (1 - max), 100, 20), Mathf.Round (max * 100) + " %");
				GUILayout.Label (new Rect (21, 20 + delta * (1 - soglia2), 100, 20), Mathf.Round (soglia2 * 100) + " %");
				GUILayout.Label (new Rect (21, 20 + delta * (1 - soglia1), 100, 20), Mathf.Round (soglia1 * 100) + " %");
				GUILayout.Label (new Rect (21, 20 + delta * (1 - min), 100, 20), Mathf.Round (min * 100) + " %");

				GUILayout.EndArea ();
			} else {
				max = 1f;
				min = 0f;
				soglia1 = 0.01f;
				soglia2 = 0.02f;
			}
			*/
			GUILayout.EndVertical ();
			GUILayout.EndArea ();	//zona di DESTRA*/





			// if button pressed or I keys
            if (GUI.Button(new Rect(10, 5, 250, 60), "Leggi le istruzioni",btStyleOFF) || Input.GetKeyUp(KeyCode.I)) {
				// set permanence timer for the force thereshold to a value
				manager.gameDataRef.forceManager.RemainingTimeSetted = 5;
				
				// set lowr limt for the force
				manager.gameDataRef.forceManager.ForceLowerLimit = 5;

				manager.SwitchState(new InstructionState(manager)); // switch the state to Setup state
            }

			// if button pressed or Q keys
			if (GUI.Button(new Rect(Screen.width - 250 - 5, 5, 250, 60), "Esci dal Gioco",btStyleOFF) || Input.GetKeyUp(KeyCode.Q)) {
				Application.Quit(); // switch the state to Setup state
			}

            // if button pressed or P keys
			if (GUI.Button(new Rect(Screen.width - 250 - 5, Screen.height - 60 - 5, 250, 60), "Gioca!",btStyleOFF) || Input.GetKeyUp(KeyCode.P)) {
				// set permanence timer for the force thereshold to a value
                manager.gameDataRef.forceManager.RemainingTimeSetted = 5;

                // set lowr limt for the force
                manager.gameDataRef.forceManager.ForceLowerLimit = 5;
				music.Stop ();
                // switch the state to Setup state
				if (tipoGioco) {
					manager.SwitchState(new PlayState(manager));
				}
				else {
					manager.SwitchState(new CoffeeState(manager));
				}
            }

			if (aiuto0) {
				GUI.Box (new Rect(Input.mousePosition.x+10,Screen.height-Input.mousePosition.y-30,400,60),"",boxInfo);
				GUI.Label(new Rect(Input.mousePosition.x+10,Screen.height-Input.mousePosition.y-30,390,60),"" +
				          "Permette di impostare il tempo impiegato dal personaggio " +
				          "per spostarsi, in fase dimostrativa, da un ostacolo all'altro.",labelStyle5);
			}
			if (aiuto1) {
				GUI.Box (new Rect(Input.mousePosition.x+10,Screen.height-Input.mousePosition.y-40,400,80),"",boxInfo);
				GUI.Label (new Rect(Input.mousePosition.x+10,Screen.height-Input.mousePosition.y-40,390,80),"" +
				           "Permette di mantenere la traccia del percorso anche mentre " +
				           "l'utente muove il personaggio, cosi facendo il giocatore " +
				           "non deve memorizzare la sequenza degli oggetti.",labelStyle5);
			}
			if (aiuto2) {
				GUI.Box (new Rect(Input.mousePosition.x+10,Screen.height-Input.mousePosition.y-40,400,80),"",boxInfo);
				GUI.Label (new Rect(Input.mousePosition.x+10,Screen.height-Input.mousePosition.y-40,390,80),"" +
				           "L'utente può condurre il personaggio all'ostacolo " +
				           "successivo premendo direttamente su questo, " +
				           "senza dover effettuare il trascinamento.",labelStyle5);
			}
			if (aiuto3) {
				GUI.Box (new Rect(Input.mousePosition.x+10,Screen.height-Input.mousePosition.y-30,400,60),"",boxInfo);
				GUI.Label (new Rect(Input.mousePosition.x+10,Screen.height-Input.mousePosition.y-30,390,60),"" +
				           "Gli ostacoli presenti sul piano di gioco saranno " +
				           "tutti di tipologie diverse.",labelStyle5);
				
			}
			if (aiuto4) {
				GUI.Box (new Rect(Input.mousePosition.x+10,Screen.height-Input.mousePosition.y-40,400,60),"",boxInfo);
				GUI.Label (new Rect(Input.mousePosition.x+10,Screen.height-Input.mousePosition.y-40,390,60),"" +
				           "Gli ostacoli una volta superati rimangono visibili " +
				           "al giocatore e possono essere utilizzati come punti " +
				           "di riferimento.",labelStyle5);
			}
			if (aiuto5) {
				GUI.Box (new Rect(Input.mousePosition.x-410,Screen.height-Input.mousePosition.y-40,400,90),"",boxInfo);
				GUI.Label (new Rect(Input.mousePosition.x-410,Screen.height-Input.mousePosition.y-40,390,90),"" +
				           "Permette di impostare i range di forza nei quali deve " +
				           "stare la pressione dell'utente per attivare specifiche azioni.\n" +
				           "    0%: forza nulla\n" +
				           "100%: forza massima",labelStyle5);
			}

            Debug.Log("In SetupState");
        }
    
		void Tooltip(string _text){
			if(Event.current.type == EventType.Repaint && GUILayoutUtility.GetLastRect().Contains(Event.current.mousePosition ))
			{} else {
				GUI.Box(new Rect(Screen.width/2,Screen.height/2,500,200),"");
				GUI.Box(new Rect(Screen.width/2,Screen.height/2,500,200),_text);
				//GUILayout.Label( "Mouse somewhere else" );
			}
		}


	}
}