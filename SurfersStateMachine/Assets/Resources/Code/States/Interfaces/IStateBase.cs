﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Resources.Code.Interfaces
{
	public interface IStateBase
	{
        /// <summary>
        /// Update is called by stateManager once per frame when state active
        /// </summary>
        void StateUpdate();
        /// <summary>
        /// called by stateManager onGUI once per frame when state active
        /// </summary>
        void ShowIt();
	}
}
