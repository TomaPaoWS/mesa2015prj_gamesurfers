﻿using UnityEngine;
using Assets.Resources.Code.Interfaces;
using Assets.Resources.Code.States;
using Assets.Resources.Code.Scripts;
using System.Collections.Generic;

namespace Assets.Resources.Code.States
{
    /// <summary>
    /// Play state. when you play the game you are here. in this class you manage all the logic of the game during playing
    /// </summary>
    /// <remarks>TO DO: put here a detailed description of this class and logic
    /// </remarks>
	class PlayState : IStateBase
    {
        /// <summary>
        /// varaible to memorize the state manager object ( core of state machine )
        /// </summary>
        private StateManager manager;
        /// <summary>
        /// raference to the shooter object
        /// </summary>
        //private GameObject shooter;
        /// <summary>
        /// memorize here the script attached to the shooter object
        /// </summary>
        //public Shooter shooterScript;
        /// <summary>
        /// reference to the force bar object
        /// </summary>
        private GameObject forceBar;
        /// <summary>
        /// memorize here the script attached to the force bar object
        /// </summary>
        public CastingBar forceBarScript;
        /// <summary>
        /// reference to the countdown timer object
        /// </summary>
        private GameObject clockTimer;
        /// <summary>
        /// memorize here the script attached the timer
        /// </summary>
        public ClockScript clockScript;
		private GameObject canvas;




		// DEFINIZIONE PARAMETRI
		public static int n_num = 6; 		//numero massimo oggetti
		int tipi_oggetto = 6;				//tipi di oggetti esistenti
		float soglia = 0.5f; 				// soglia radianti tra rette, più è alta meno l'ostacolo può trovarsi in mezzo alla traiettoria
		float rapp = (float)(n_num + 1); 	//indica la frazione della diagonale d'ambiente che da la vicinanza max tra oggetti
		float v_linea = 3f;					//velocità movimento personaggio fase 1			
		float cornice = 0.65f;				//zona proibita per gli ostacoli sul bordo del campo di gioco
		float sei_stupido = 15f;			//soglia di tempo da cui attivare l'aiuto per individuare l'ostacolo
		float attesa_start = 5f;			//attesa per cominciare la fase 1 (il personaggio si scalda)
		float attesa_jump = 0.1f;			//attesa del personaggi oprima di eseguire il salto
		float attesa_fase2 = 3f;			//attesa tra la fine della fase 1 e l'inizio della fase 2
		float attesa_risultati = 4f;		//attesa per plottare i risultati al termine del gioco
		float soglia_pick = 2f;				//raggio di circonferenza entro il quale, premendo, il personaggio si porta sul punto
		float fade_ostacoli =  3f;			//velocità dissolvenza ostacoli	
		float Tcam = 4f;					//durata del movimento finale della telecamera
		float Tcamv = 4f;					//durata del movimento iniziale della telecamera

		Vector3 sut = new Vector3 (0f, 0.2f, 0f);					//altezza delle linee sul piano
		Vector3 vc_p = new Vector3 (35.1f, 19f, -30.8f); 			//posizione iniziale della telecamers
		Vector3 vc_r = new Vector3 (30f, -40f, 0f);					//rotazione iniziale telecamera
		Vector3 startc_p = new Vector3 (-0.38f, 10.23f, -9.07f); 	//posizione di gioco telecamera
		Vector3 stopc_p = new Vector3 (4.28f, 2.11f, 0f);			//posizione finale telecamera
		Vector3 startc_r = new Vector3 (52f, 0f, 0f);				//rotazione di gioco telecamera		
		Vector3 stopc_r = Vector3.zero;								//rotazione finale telecamera

		//PARAMETRI OSTACOLI
		// moto,panchina,tronchi,birilli,basket,calcio
		float[] r_circT = {1.7f,1.55f,1.5f,0.8f,0.6f,0.6f};			//raggi delle circonferenze
		float[] altezzeT = {0.8f,1f,0.6f,0.5f,0.7f,0.4f};			//altezze dei salti
		int[] tipo_saltoT = {1,1,1,0,0,0};							//tipo di salto per i vari oggetti

		//AUDIO
		AudioSource music;						//sorgente audio 
		AudioClip canzone,saltom,vittoriam;		//clip relative a canzone di gioco, ostacolo corretto e vittoria

		//LINEE
		GameObject[] lineeg = new GameObject[n_num + 1]; 	//oggetti a cui assegnare i renderer delle linee
		LineRenderer[] linee = new LineRenderer[n_num + 1];	//renderer linee
		float[] track = new float[n_num + 1];				//lunghezze delle linee

		//AMBIENTE
		GameObject ambiente; 						//a cui si associa il prefab dell'ambiente
		Ostacolo[] oggetti = new Ostacolo[n_num]; 	//array di istanze di tipo ostacolo
		Personaggio boy;							//istanza del personaggio
		Camera myCamera;							//camera da cui è visulizzato il gioco

		//AUSILIARI
		int n_o; 										//numero oggetti in gioco
		Vector3 tVector1,tVector2,tVector3;				//vettori utilizzati nell'algoritmo di posizionamento
		Vector3 p; 										//vettore che indica la posizione indicata dal cursore
		Vector3[] posizione = new Vector3[n_num + 2];	//array delle posizione dei vari oggetti
		float rx,rz,ang1,ang2,ang3,dist;				//variabili per l'algoritmo assegnazione posizioni
		float t,ssx,ssy,ssz;							//variabili per l'individuazione della posizione el cursore
		float[] saveOggetti = new float[n_num];			//array in cui salvare i tipi dei vari oggetti
		float[,] forzeT = new float[8,2];				//array degli estremi di forza popolata dal menu
		int tOggetto;									//definisce il tipo di oggetto
		bool c1m,c2m;									//condizioni dell'algoritmo di posizionamento
		int stato;										//stato della macchina stati interna
		float actualTime,actualForce;					//informazioni su tempo e pressione esercitata
		Vector3 initialPos;								//posizione di partenza personaggio
		float tempoOstacoli;							//tempo passato dalla partenza da un ostacolo
		Vector3 lastPos;
		ForceManager.InThreshold valorePrima;			//variabile per il salvataggio della pressione


		bool girotondoInEsecuzione,raggiungiInEsecuzione,SaltoInEsecuzione;  //booleani relativi alle fasi delle animazioni



		//CONTATORI 0 FLAG
		float counter;		//utilizzato nella prima fase per determinare a che punto si trova il personaggio sul segmento
		int conttel = 0;	//contatore per il movimento della telecamera
		float t_stupido;	//variabile per misurare da quanto tempo il personaggio sta cercando l'ostacolo
		int cont,flag;
		bool firstTime=true;
		bool inizioPressione = false;
		float[] parFloat;
		bool[] parBool;
		bool lanciatoSuOstacolo=false;


		//VARIABILI PER I RISULTATI
		Vector3[] r_startPos = new Vector3[n_num + 1];		//array posizioni iniziali
		Vector3[] r_endPos = new Vector3[n_num + 1];		//array posizioni finali
		float[] r_spacePC = new float[n_num + 1];			//array distanze pc
		float[] r_spaceHU = new float[n_num + 1];			//array distanze utente


		float[] r_startTimePC = new float[n_num + 1];		//array istanti iniziali pc
		float[] r_endTimePC = new float[n_num + 1];			//array istanti finali pc
		float[] r_startTimeHU = new float[n_num + 1];		//array istanti iniziali utente
		float[] r_endTimeHU = new float[n_num + 1];			//array istanti finalli utente

		bool r_primoGiusto = false;							//info su individuazione primo ostacolo						
		bool r_ultimoGiusto = false;						//info su individuazione ultimo ostacolo
		bool[] r_ostacoloCorretto = new bool[n_num + 1];	//array esito ostacoli

		float r_rilasciTrascinamento = 0f;					//rilasci trascinamento
		float r_rilasciSalto = 0f;							//rilasci salto



        /// <summary>
        /// Create the state and load the Scene01
        /// </summary>
        /// <param name="managerRef"> store the state manager object reference</param>
        /// <remarks>when initialize play state we also take the reference to the player object and to 
        /// objects for the force management ( force bar and clock timer )</remarks>
        public PlayState(StateManager managerRef)    // constructor
        {
            
			manager = managerRef;   // store a reference to StateManager class

			CaricaScena ();			

			myCamera = Camera.main;
			myCamera.clearFlags = CameraClearFlags.Skybox;


			//IMPOSTAZIONE DEI PARAMETRI CON I VALORI INSERITI DA MENU'

			//numero ostacoli
			n_o = Mathf.RoundToInt(manager.gameDataRef.parFloat [6]); 	
				
			// estremi range forza
			forzeT[0,0] = manager.gameDataRef.parFloat [4];						
			forzeT[1,0] = manager.gameDataRef.parFloat [4];
			forzeT[2,0] = manager.gameDataRef.parFloat [4];
			forzeT[3,0] = manager.gameDataRef.parFloat [2];
			forzeT[4,0] = manager.gameDataRef.parFloat [2];
			forzeT[5,0] = manager.gameDataRef.parFloat [2];
			forzeT[0,1] = manager.gameDataRef.parFloat [5];
			forzeT[1,1] = manager.gameDataRef.parFloat [5];
			forzeT[2,1] = manager.gameDataRef.parFloat [5];
			forzeT[3,1] = manager.gameDataRef.parFloat [3];
			forzeT[4,1] = manager.gameDataRef.parFloat [3];
			forzeT[5,1] = manager.gameDataRef.parFloat [3];

			//tempo tra gli ostacoli
			tempoOstacoli = manager.gameDataRef.parFloat [7];	

			// altri parametri
			parFloat = manager.gameDataRef.parFloat;
			parBool = manager.gameDataRef.parBool;

			//AUDIO

			music = myCamera.GetComponent<AudioSource> ();	//individuo la sorgente audio di scena
			//importo le clip
			canzone = GameObject.Instantiate (UnityEngine.Resources.Load ("musicaSalta")) as AudioClip;
			saltom = GameObject.Instantiate (UnityEngine.Resources.Load ("SSalto")) as AudioClip;
			vittoriam = GameObject.Instantiate (UnityEngine.Resources.Load ("Vittoria")) as AudioClip;

			music.clip = canzone; 	//assegno la clip della canzone di sottofondo, sarà eseguita in loop

			if (!manager.gameDataRef.parBool[6]) {
				music.Play ();	//play della musica se i suoni sono abilitati
			}


			//CAMERA IN POSIZIONE INIZIALE
			myCamera.transform.position = vc_p; 
			myCamera.transform.eulerAngles = vc_r;

		
			// INIZIALIZZAZIONE AMBIENTE

			//importo il prefab e lo posiziono
			ambiente = GameObject.Instantiate (UnityEngine.Resources.Load("ambiente")) as GameObject;
			ambiente.transform.position = Vector3.zero;
			ambiente.transform.eulerAngles = Vector3.zero;

			//assegno alla prima e all'ultima posizione le coordinate dei bollini verde e rosso
			posizione[0] = ambiente.transform.GetChild (0).position;
			posizione[n_o+1] = ambiente.transform.GetChild (1).position;
			
			//definisco la minima distanza tra gli ostacoli in funzione del numero di questi
			dist = Mathf.Max (Vector3.Distance(posizione[0],posizione[n_o+1])/rapp,2f*Mathf.Max(r_circT));
			


			//ASSEGNAZIONE PRIMO OGGETTO
			flag = 0;
			while (flag == 0) { //si esce nel caso di distanze con i bollini rispettate
				rx  = Random.Range (posizione[0].x+cornice, posizione[n_o+1].x-cornice);
				rz  = Random.Range (posizione[0].z+cornice, posizione[n_o+1].z-cornice);
				if ((Vector3.Distance(new Vector3(rx,0f,rz),posizione[0]) > 2f*dist) && (Vector3.Distance(new Vector3(rx,0f,rz),posizione[n_o+1]) > dist)) {
					flag = 1; } 
			}

			//salvo posizione 
			posizione [1] = new Vector3 (rx , 0f, rz );
			tVector1 = posizione[1] - posizione[0];

			//scelta oggetto
			tOggetto = Random.Range (0, tipi_oggetto);
			saveOggetti [0] = tOggetto;
			
			//creazione oggetto
			oggetti [0] = new Ostacolo (posizione [1], new Vector3 (0f, Mathf.Rad2Deg * (-Mathf.Atan2 (tVector1.z, tVector1.x)), 0f), "Oggetto" + (tOggetto + 1), r_circT [tOggetto], forzeT [tOggetto,0], forzeT [tOggetto,1],altezzeT [tOggetto],tipo_saltoT[tOggetto]);
			
			//creo e posiziono personaggio
			boy = new Personaggio (posizione[0],posizione[1]);
			r_startPos[0] = posizione[0];


			//ASSEGNAZIONE DEGLI ALTRI OGGETTI
			for (int i=2; i < n_o+1; i++) {	//ciclo sui vari oggetti
				flag = 0;
				int contatore = 0;
				while (flag == 0) {	//ciclo per verificare il rispetto dei vincoli
					if (contatore > 300) { //dopo un certo numero di cicli viene riassegnata la prima posizione

						//riassegnazione primo oggetto
						contatore = 0;
						GameObject[] ogg = GameObject.FindGameObjectsWithTag("ostacolo");
						foreach (GameObject element in ogg) {
							GameObject.Destroy(element); //distruzione ostacoli
						}
						flag = 0;
						while (flag == 0) {
							rx  = Random.Range (posizione[0].x+cornice, posizione[n_o+1].x-cornice);
							rz  = Random.Range (posizione[0].z+cornice, posizione[n_o+1].z-cornice);
							if ((Vector3.Distance(new Vector3(rx,0f,rz),posizione[0]) > 2f*dist) && (Vector3.Distance(new Vector3(rx,0f,rz),posizione[n_o+1]) > dist)) {
								flag = 1; } 
						}
						posizione [1] = new Vector3 (rx , 0f, rz );
						tVector1 = posizione[1] - posizione[0];
						tOggetto = Random.Range (0, tipi_oggetto);
						saveOggetti [0] = tOggetto;
						oggetti [0] = new Ostacolo (posizione [1], new Vector3 (0f, Mathf.Rad2Deg * (-Mathf.Atan2 (tVector1.z, tVector1.x)), 0f), "Oggetto" + (tOggetto + 1), r_circT [tOggetto], forzeT [tOggetto,0], forzeT [tOggetto,1],altezzeT [tOggetto],tipo_saltoT[tOggetto]);

						i = 2; // si riparte con l'assegnazione degli altri oggetti
					}
					flag = 1;
					rx = Random.Range (posizione [0].x + cornice, posizione [n_o + 1].x - cornice);
					rz = Random.Range (posizione [0].z + cornice, posizione [n_o + 1].z - cornice);
					posizione [i] = new Vector3 (rx, 0f, rz);

					//controllo distanze
					for (int j = 0; j < i-1; j++) {
						//distanza da oggetti
						if (Vector3.Distance (new Vector3 (rx, 0f, rz), posizione [j]) < dist) {
							flag = 0;
						}
						//distanza da oggetto precedente
						if (Vector3.Distance (new Vector3 (rx, 0f, rz), posizione [i - 1]) < 2f * dist) {
							flag = 0;
						}
						//distanza da traguardo finale
						if (Vector3.Distance (new Vector3 (rx, 0f, rz), posizione [n_o + 1]) < dist) {
							flag = 0;
						}
					}
					
					//controllo posizioni proibite
					if (flag == 1) {
						//controllo conflitti con oggetti già esistenti
						for (int j=0; j<i-1; j++) {
							//vettore oggetto -> altro_oggetto
							tVector1 = posizione [j] - posizione [i];
							ang1 = Mathf.Atan2 (tVector1.z, tVector1.x);
							ang1 = (ang1 + Mathf.PI * 2f) % (Mathf.PI * 2f);

							//vettore oggetto -> altro_oggetto+1
							tVector2 = posizione [j + 1] - posizione [i];
							ang2 = Mathf.Atan2 (tVector2.z, tVector2.x);
							ang2 = (ang2 + Mathf.PI * 2f) % (Mathf.PI * 2f);

							//vettore oggetto-1 -> altro_oggetto
							tVector3 = posizione [j] - posizione [i - 1];
							ang3 = Mathf.Atan2 (tVector3.z, tVector3.x);
							ang3 = (ang3 + Mathf.PI * 2f) % (Mathf.PI * 2f);

							//controllo di soglia sulla condizione1: nuovo oggetto tra due già esistenti
							c1m = Mathf.Abs (Mathf.Abs (ang1 - ang2) - Mathf.PI) < soglia;
							
							//controllo di soglia sulla condizione1: nuovo oggetto dietro a un oggetto già esistente
							c2m = Mathf.Abs (Mathf.Abs ((ang1 + Mathf.PI) % (Mathf.PI * 2f) - (ang3 + Mathf.PI) % (Mathf.PI * 2f)) - Mathf.PI) < soglia;
							
							//eventuale rifiuto posizione
							if (c1m || c2m) {
								flag = 0;
							}
							
							//se è l'ultimo oggetto va controllata anche posizione rispetto a traguardo
							if (i == n_o && flag == 1) {
								//vettore altro_oggetto+1 -> traguardo finale
								tVector1 = posizione [n_o + 1] - posizione [j + 1];
								ang1 = Mathf.Atan2 (tVector1.z, tVector1.x);
								ang1 = (ang1 + Mathf.PI * 2f) % (Mathf.PI * 2f);

								//tra il nuovo oggetto e il traguardo non deve esserci un altro ostacolo
								c1m = Mathf.Abs (Mathf.Abs (ang1 - (ang2 + Mathf.PI) % (Mathf.PI * 2f)) - Mathf.PI) < soglia;

								//eventuale rifiuto posizione
								if (c1m) {
									flag = 0;
								}
							}
						}
					}
					contatore += 1; //conteggio per uscita dal loop in caso di stallo
				}		
				
				//scelgo tipo d'oggetto e creo il nuovo ostacolo
				tVector1 = posizione [i] - posizione [i - 1];
				
				//se gli oggetti si vogliono tutti diversi va evitata la ridondanza
				if(manager.gameDataRef.parBool [4]==false){
					tOggetto = Random.Range (0, tipi_oggetto); //ostacoli anche uguali
				} else{ //ostacoli sempre diversi
					bool notGood = true;
					while (notGood){
						tOggetto = Random.Range (0, tipi_oggetto);
						notGood=false;
						for (int j=0;j<i-1;j++){
							if(tOggetto==saveOggetti[j]){
								notGood=true;
							}
						}
					}
					saveOggetti[i-1] = tOggetto; //salvo il tipo di oggetto
				}
				
				//creazione oggetto, posizonamento, assegnazione forza, altezza, tipo salto
				//l'oggetto è orientato perpendicolarmente alla traiettoria
				oggetti [i - 1] = new Ostacolo (posizione [i], new Vector3 (0f, Mathf.Rad2Deg * (-Mathf.Atan2 (tVector1.z, tVector1.x)), 0f), "Oggetto" + (tOggetto + 1), r_circT [tOggetto], forzeT [tOggetto, 0], forzeT [tOggetto, 1], altezzeT [tOggetto], tipo_saltoT [tOggetto]);
			}
				

			//INIZIALIZZAZIONE LINEE PERCORSO
			//importazione del prefab linea e assegnazione ai GameObject
			for (int i=0; i<n_o+1; i++) {
				lineeg[i] = GameObject.Instantiate (UnityEngine.Resources.Load ("linea")) as GameObject;
				lineeg[i].tag = "turbine"; //attenzione, le linee sono quelle delle traiettorie
				linee[i] = lineeg[i].GetComponent<LineRenderer>();
				linee[i].SetWidth(0.1f,0.1f);
			}


			//linee collassate nei punti iniziali dei segmenti
			track[0] = Vector3.Distance(posizione[0],posizione[1]);
			linee[0].SetPosition(0,posizione[0]+sut);
			linee[0].SetPosition(1,posizione[0]);
			for (int i=1; i<n_o+1; i++) {
				linee[i].SetWidth(0.1f,0.1f);
				track[i] = Vector3.Distance(posizione[i],posizione[i+1]);
				linee[i].SetPosition(0,(posizione[i+1]-posizione[i])*(oggetti[i-1].r_circ/track[i])+posizione[i]+sut);
				linee[i].SetPosition(1,(posizione[i+1]-posizione[i])*(oggetti[i-1].r_circ/track[i])+posizione[i]+sut);
			}
				

			//AZZERMENTO CONTATORI
			cont = 0;
			counter = 0;
			conttel = 0;

			//INIZIALMENTE TUTTI OSTACOLI CORRETTI
			for (int h=0; h<n_num; h++) {
				r_ostacoloCorretto[h] = true;
			}

			//IMPOSTAZIONE STATO INIZIALE
			stato = -1;
				

	
	} //FINE COSTRUTTORE PLAYSTATE

        /// Update is called by stateManager once per frame when state active
        public void StateUpdate()
		{	

            // if mouse is present update the force value
            if (Input.mousePresent && (stato == 8 || stato==10)) {

				// if Arduino connected simulate force with it
				if (false){//manager.sp.IsOpen) {                
					// pass to the function the bool value to simulate decrease and increase of force
					manager.gameDataRef.forceManager.UpdateForce (manager.gameDataRef.ArduinoButtonA, manager.gameDataRef.ArduinoButtonB,manager.gameDataRef.parBool[5]);
					actualForce = manager.gameDataRef.forceManager.actualForce / 1000;
				}
                // pass to the function the bool value to simulate decrease and increase of force using keyboard
                else {
					manager.gameDataRef.forceManager.UpdateForce (Input.GetKey (KeyCode.A), Input.GetKey (KeyCode.B),manager.gameDataRef.parBool[5]);
					actualForce = manager.gameDataRef.forceManager.actualForce / 1000;
				}
				//if timer is active update it
				if (manager.gameDataRef.forceManager.IsTimerActive) {
					float actualRemainingTime = manager.gameDataRef.forceManager.remainingTime;
					manager.gameDataRef.forceManager.updateTimer();
					clockScript.UpdateClock ((manager.gameDataRef.forceManager.remainingTime - actualRemainingTime) * 360 / manager.gameDataRef.forceManager.RemainingTimeSetted);
				}
                //else if I'm in threshold and at costant value start it
				else if (manager.gameDataRef.forceManager.InThresholdValue == ForceManager.InThreshold.inner && manager.gameDataRef.forceManager.InStdDevForveLimit && stato==10) {
					manager.gameDataRef.forceManager.StartTimer ();
					clockTimer.SetActive (true);
				}
				forceBarScript.UpdateCastBar (manager.gameDataRef.forceManager.actualForce); // set the actual force value in the bar
				forceBarScript.UpdateCastBarColor ((int)manager.gameDataRef.forceManager.InThresholdValue); // update force bar color
			} else {

			}       

            // if timer is inactive hide the clock timer and reset it
            if (!manager.gameDataRef.forceManager.IsTimerActive) {
				clockScript.restArrow ();
				clockTimer.SetActive (false);
			}	else {
				Debug.Log("*******************TIMER-ATTIVO: "+manager.gameDataRef.forceManager.remainingTime);

			}



			int statoApp = stato; 	//salvo lo stato dell'update precedente	


			switch(stato) { //MACCHINA STATI INTERNA
			
			case -1: //telecamera si muove mostrando l'ambiente per portarsi nell'inquadratura di gioco

				//fino al raggiungimento del punto finale eseguo interpolazione utilizzando un contatore
				if (Vector3.Distance (myCamera.transform.position, startc_p) > 0.001f) {
					myCamera.transform.position = new Vector3(Mathf.Lerp(vc_p.x,startc_p.x,conttel*(0.03f/Tcamv)),Mathf.Lerp(vc_p.y,startc_p.y,conttel*(0.03f/Tcamv)),Mathf.Lerp(vc_p.z,startc_p.z,conttel*(0.03f/Tcamv)));
					myCamera.transform.eulerAngles = new Vector3 (Mathf.Lerp(vc_r.x,startc_r.x,conttel*(0.03f/Tcamv)),Mathf.Lerp(vc_r.y,startc_r.y,conttel*(0.03f/Tcamv)),Mathf.Lerp(vc_r.z,startc_r.z,conttel*(0.03f/Tcamv)));
					
					
					conttel += 1;
				}
				else { //posizione inquadratura di gioco raggiunta
					conttel = 0;
					stato = 0;
				}
				break;
			
			case 0: //personaggio fa riscaldamento per qualche secondo
				boy.ANIM_risc = true;
			if(firstTime) { 
				actualTime = Time.time;
					myCamera.transform.position =startc_p;
					myCamera.transform.eulerAngles = startc_r;
			}
			if(Time.time > actualTime+attesa_start) {
				boy.ANIM_risc = false;
				stato = 1;
			}

				break;
				
			case 1: //personaggio si muove verso un ostacolo FASE 1




				if (cont < n_o) { //se non deve raggiungere l'arrivo
					oggetti[cont].changecolor(true); //circonferenza dell'ostacolo cambia colore
					oggetti[cont].psyco(Time.time,2f); //circonferenza dell'ostacolo ruota

					if(firstTime) {//se è primo ingresso nel caso 
						r_startTimePC[cont] = Time.time; //parte conteggio tempo
						initialPos = boy.myPersonaggio.transform.position; //salvo posizione partenza

						//se tempoOstacoli e libero, tempo scelto random
						if (tempoOstacoli < 0.1f) {tempoOstacoli = Random.Range(2,5);}

						//assegno v_linea in base al tempo di percorrenza richiesto
						v_linea = (Vector3.Distance(posizione[cont+1],initialPos)-oggetti[cont].r_circ)/tempoOstacoli/0.92f;

						//personaggio condotto nella posizione dell'ostacolo da raggiungere
						boy.Raggiungi(posizione[cont+1],v_linea,oggetti[cont].r_circ);
					}

					//disegna linea solo all'esterno delle circonferenza
					if (counter < track[cont]-oggetti[cont].r_circ) { 
						counter = Vector3.Magnitude(boy.myPersonaggio.transform.position - posizione[cont]) ;
						//assegno punto finale della linea (posizione personaggio)
						linee [cont].SetPosition (1,boy.myPersonaggio.transform.position + sut);
					}

					// quando è arrivato all'ostacolo
					if (boy.Raggiungi(posizione[cont+1],v_linea,oggetti[cont].r_circ)) { 

						r_endPos[cont] = boy.myPersonaggio.transform.position; //salvo end position
						r_endTimePC[cont] = Time.time; //salvo end time
						counter = 0f; //azzero contatore
						stato = 2;
					}					
				
				
				} else { //deve raggiungere l'arrivo
					if(firstTime) { 
						//stesse operazione come per gli altri ostacoli
						r_startTimePC[cont] = Time.time;
						initialPos = boy.myPersonaggio.transform.position;
						if (tempoOstacoli < 0.1f) {tempoOstacoli = Random.Range(2,5);} 
						v_linea = (Vector3.Distance(posizione[cont+1],initialPos)-0.1f)/tempoOstacoli/0.91f;
						boy.Raggiungi(posizione[cont+1],v_linea,0.1f);
						}
					
					if (counter < track[cont]) { //disegna linea fino in fondo
						counter = Vector3.Magnitude(boy.myPersonaggio.transform.position - posizione[cont]) ;
						linee [cont].SetPosition (1,boy.myPersonaggio.transform.position + sut);
					}

					if (boy.Raggiungi()) {//quando è arrivato all'arrivo
						r_endPos[cont] = boy.myPersonaggio.transform.position; //salvo end position
						r_endTimePC[cont] = Time.time; //salvo end time
						stato = 5;
					}
				}
				break;
				
			case 2: //piccola pausa mentre il personaggio carica il salto durante FASE 1
				boy.ANIM_carSalto = true;
				if(firstTime) { 
					actualTime = Time.time;
				}
			if(Time.time > actualTime+attesa_jump) {
				boy.ANIM_carSalto = false;
				stato = 3;
			}
				break;
				
			case 3: //personaggio salta durante FASE 1
				//salta di una lunghezza pari al diametro della circonferenza
				//vengono rispettati anchei vincoli su tipo di salto e altezza
				if(firstTime) { boy.Salta(oggetti[cont].altezza,oggetti[cont].r_circ*2f,oggetti[cont].salto); }
				if (boy.Salta ()) { //quando salto è finito
					stato = 4;
				}
				break;
				
			case 4: //personaggio fa il girotondo all'ostacolo appena saltato durante FASE 1
				if (firstTime) { 
					r_startPos[cont+1] = boy.myPersonaggio.transform.position;
					boy.Girotondo(posizione[cont+1],posizione[cont+2],true); }
				if (boy.Girotondo()) { //quando girotondo è finito
					oggetti[cont].changecolor(false); //circonferenza torna al coloro standard
					cont = cont + 1;
					stato = 1;
				}
				break;
				
			case 5: //personaggio esulta e lascia passare del tempo
			boy.ANIM_fest = true;
				if (firstTime) {boy.Guarda(myCamera.transform.position); }
				if(firstTime) { 
					actualTime = Time.time;
				}
			if(Time.time > actualTime+attesa_fase2) {
				boy.ANIM_fest = false;
				stato = 6;}
				break;
				
			case 6: //fine fase 1

				//distruzione linee a seconda delle opzioni scelte
				if (manager.gameDataRef.parBool [1]==false){
					for (int i=0; i<n_o+1; i++) {
						GameObject.Destroy(lineeg [i]); 
					}
				}

				GameObject.Destroy(boy.myPersonaggio); //distruzione personaggio
				boy = new Personaggio (posizione[0],posizione[1]); //creazione nuovo personaggio
				canvas.SetActive(true);
				boy.ANIM_risc = true;

				//azzeramento contatori e impostazione stato
				cont = 0;
				stato = 8;
				r_startTimeHU[0] = Time.time; //salvataggio tempo start

				break;
				
			case 7: //caso di riserva
				break;
				
			//FASE 2 
			case 8: //personaggio segue il mouse fino al prossimo ostacolo. 
				if(firstTime){ //primo ciclo
					//forze scalate
					manager.gameDataRef.forceManager.ForceLowerLimit = manager.gameDataRef.parFloat[0]*1000;
					manager.gameDataRef.forceManager.ForceUpperLimit = manager.gameDataRef.parFloat[1]*1000;
					forceBarScript.InitializeForceBar(manager.gameDataRef.forceManager.ForceLowerLimit, manager.gameDataRef.forceManager.ForceUpperLimit, manager.gameDataRef.forceManager.actualForce+0.001f);    // initialize force bar value
					//parte conteggio per attivazione aiuto
					t_stupido = Time.time;
					lanciatoSuOstacolo=false;  //di default npn si punta ostacolo
					lastPos = r_startPos[cont]; //salvo ultima posizione, ovvero lo start
				}

				//attivazione aiuto dopo un certo tempo
				if (Time.time-t_stupido > sei_stupido && cont < n_o) {
					oggetti[cont].changecolor(true); //cambio colore
					oggetti[cont].psyco(Time.time,2f); //rotazione
				}

				// conto i rilasci durante il trascinamento
				if (manager.gameDataRef.forceManager.InThresholdValue == ForceManager.InThreshold.under && valorePrima == ForceManager.InThreshold.inner) {
					r_rilasciTrascinamento += 1f;
					Debug.Log("rilasci: "+r_rilasciTrascinamento);
				}

				//salvo ultimo valore forza
				valorePrima = manager.gameDataRef.forceManager.InThresholdValue;


				//individuazione del punto mouse
				if (manager.gameDataRef.forceManager.InThresholdValue == ForceManager.InThreshold.inner) { //forza sopra prima soglia
					boy.ANIM_risc = false; // non fa riscaldamento
					//vettore del raggio individuato dal cursore
					ssx = myCamera.ScreenPointToRay(Input.mousePosition).direction.x;
					ssy = myCamera.ScreenPointToRay(Input.mousePosition).direction.y;
					ssz = myCamera.ScreenPointToRay(Input.mousePosition).direction.z;
					//vincolo del piano y = 0
					t = -myCamera.transform.position.y/ssy;
					//individuazione delle coordinate della posizione
					p = new Vector3(myCamera.transform.position.x + t*ssx,0f,myCamera.transform.position.z + t*ssz);
					
					//distanza personaggio-cursore
					float distance = Vector3.Magnitude(p-boy.myPersonaggio.transform.position); 

					//caso in cui si può puntare direttamente all'ostacolo
					if (manager.gameDataRef.parBool [2]){
						lanciatoSuOstacolo = true;
						soglia_pick = 200f; //soglia pick impostata altissima
						distance = 1f;
					}

					//se giocatore ha premuto abbastanza vicino
					if (distance < soglia_pick) {
						r_spaceHU[cont] += Vector3.Distance(lastPos,boy.myPersonaggio.transform.position); //salvo tratto percorso
						lastPos = boy.myPersonaggio.transform.position;//salvo posizione
						boy.Raggiungi(p,5f*distance,0.1f); //personaggio portato nel punto, con velocita funzione di distanza
					} else { // l'utente ha premuto troppo lontano
						boy.Stop(); //stop
					}

				} else { // l'utente non ha premuto sullo schermo
					if(lanciatoSuOstacolo){
						r_spaceHU[cont] += Vector3.Distance(lastPos,boy.myPersonaggio.transform.position);
						lastPos = boy.myPersonaggio.transform.position;
						boy.Raggiungi();
					} else {
					boy.Stop(); //stop
					}
				}


				//valuta se ha sbagliato l'ostacolo
				for (int h=0; h<cont; h++){ //ne ha preso uno precedente
					if (Vector3.Distance(p,posizione[h])<(oggetti[h].r_circ/2f)){
						r_ostacoloCorretto[cont] = false; //segnalo l'esito negativo

					}
				}
				for (int h=cont+1; h<n_o; h++){ //ne ha preso uno successivo
					if (Vector3.Distance(p,posizione[h+1])<(oggetti[h].r_circ/2f)){
						r_ostacoloCorretto[cont] = false; //segnalo l'esito negativo

					}
				}

				// gestione dello stop
				if (cont < n_o) { // se non deve raggiungere l'arrivo
					//se arrivo sulla circonferenza dell'oggetto giusto
					if (Vector3.Magnitude(posizione[cont+1]-boy.myPersonaggio.transform.position) < oggetti[cont].r_circ) {
						oggetti[cont].changecolor(false); //ritorno a colore normale
						r_endTimeHU[cont] = Time.time; //end tempo
						boy.Stop(); //stop
						boy.Girotondo(posizione[cont+1],posizione[cont],false); //faccio girotondo per portarmi in posizione
						cont = cont + 1; //contatore passa a oggetto successivo
						stato = 9;
					}
				} else { // se deve raggiungere l'arrivo
					//se sono arrivato
					if (Vector3.Magnitude(posizione[cont+1]-boy.myPersonaggio.transform.position) < 0.3f) {
						r_endTimeHU[cont] = Time.time;
						boy.Stop();
						conttel = 0;
						stato = 15;
					}
				}
				break;
				
			case 9: //personaggio si riallinea prima di saltare
				if(boy.Girotondo()) { //se il girotondo è terminato

					stato = 10;
				}
				break;
				
		case 10:
			if(firstTime){
					manager.gameDataRef.forceManager.ForceLowerLimit = oggetti[cont-1].forza*1000;//minimo
					manager.gameDataRef.forceManager.ForceUpperLimit = oggetti[cont-1].forzaLim*1000;//massimo
					// initialize force bar value
					forceBarScript.InitializeForceBar(manager.gameDataRef.forceManager.ForceLowerLimit, manager.gameDataRef.forceManager.ForceUpperLimit, manager.gameDataRef.forceManager.actualForce+0.001f);    
				}
			float val = Mathf.Min(1f,actualForce/oggetti[cont-1].forza); //si salva la forza necessaria per questo ostacolo
			boy.Carica(val);//personaggio carica il salto
				//verifica forza e tempo corretto
				if (manager.gameDataRef.forceManager.remainingTime<0.15f && manager.gameDataRef.forceManager.remainingTime>0.01f) { 
					boy.ANIM_carSalto = false;
					stato = 13;
			}
			
			break;
		
		case 11: //caso di riserva
			break;
				
		case 12: //caso di riserva
			break;
				
			case 13: // personaggio salta e supera l'ostacolo
				if (firstTime) { 
					if (!manager.gameDataRef.parBool[6]) { //se suoni abilitati
						music.PlayOneShot(saltom); //suono di salto corretto effettuato
					}
					//il personaggio salta con in vincoli relativi a tipo di salto altezza e lunghezza in funzione dell oggetto
					boy.Salta(oggetti[cont-1].altezza,oggetti[cont-1].r_circ*2f,oggetti[cont-1].salto); }
				if (boy.Salta()) {//se salto è finito
					if (manager.gameDataRef.parBool [3]==false){//se è abilitata distruzione ostacoli
						oggetti[cont-1].distruggi(fade_ostacoli); //parte il fade degli ostacoli
					}

					stato = 14;
				}
				break;
				
			case 14: //distruzione ostacoli
				if (oggetti[cont-1].vuoto == false && manager.gameDataRef.parBool [3]==false) { 
					oggetti[cont-1].distruggi(fade_ostacoli);
				}
				else {
					r_startTimeHU[cont] = Time.time; //start time
					stato = 8;
				}
				break;
				
			case 15: // arrivo al bollino rosso
				canvas.SetActive(false);
				boy.ANIM_fest = true;
				
				if(firstTime) { 
					if (!manager.gameDataRef.parBool[6]) {
						music.PlayOneShot(vittoriam); // appena arrivo parte suono vittoria
					}
					Debug.Log ("FINITO TUTTO YEEE!");
				}
				//camera si sposta nella posizione finale
				if (Vector3.Distance (myCamera.transform.position, stopc_p) > 0.001f) {
					myCamera.transform.position = new Vector3(Mathf.Lerp(startc_p.x,stopc_p.x,conttel*(0.03f/Tcam)),Mathf.Lerp(startc_p.y,stopc_p.y,conttel*(0.03f/Tcam)),Mathf.Lerp(startc_p.z,stopc_p.z,conttel*(0.03f/Tcam)));
					myCamera.transform.eulerAngles = new Vector3 (Mathf.Lerp(startc_r.x,stopc_r.x,conttel*(0.03f/Tcam)),Mathf.Lerp(startc_r.y,stopc_r.y,conttel*(0.03f/Tcam)),Mathf.Lerp(startc_r.z,stopc_r.z,conttel*(0.03f/Tcam)));
					
					boy.Guarda(Camera.main.transform.position);
					conttel += 1;
				} else {
					stato = 16;
				}
			
				break;
			case 16: // calcolo dei risultati

				//azzero risultati
				manager.gameDataRef.risultFloat[0] = 0f;
				manager.gameDataRef.risultFloat[1] = 0f;
				manager.gameDataRef.risultFloat[2] = 0f;
				manager.gameDataRef.risultFloat[3] = 0f;
				manager.gameDataRef.risultFloat[4] = 0f;
				manager.gameDataRef.risultFloat[5] = 0f;
				manager.gameDataRef.risultFloat[6] = 0f;

				//composizione risultati
				for(int h=0;h<=n_o;h++){
					manager.gameDataRef.risultFloat[0] += r_endTimePC[h] - r_startTimePC[h];
					manager.gameDataRef.risultFloat[1] += r_endTimeHU[h] - r_startTimeHU[h];
					manager.gameDataRef.risultFloat[2] += Vector3.Distance(r_startPos[h],r_endPos[h]);
					manager.gameDataRef.risultFloat[3] += r_spaceHU[h];
					if(r_ostacoloCorretto[h]) {manager.gameDataRef.risultFloat[6] += 1f;}
				}

				//print dei risultati
				manager.gameDataRef.risultFloat[4] = r_rilasciTrascinamento;
				Debug.Log( "timePC: "+manager.gameDataRef.risultFloat[0]);
				Debug.Log( "timeHU: "+manager.gameDataRef.risultFloat[1]);
				Debug.Log("spacePC: "+manager.gameDataRef.risultFloat[2]);
				Debug.Log("spaceHU: "+manager.gameDataRef.risultFloat[3]);
				Debug.Log("rilasci: "+manager.gameDataRef.risultFloat[4]);
				Debug.Log("corretti: "+manager.gameDataRef.risultFloat[6]);

				//primo e ultimo ostacolo corretti o meno
				manager.gameDataRef.risultBool[0] = r_ostacoloCorretto[0];
				manager.gameDataRef.risultBool[1] = r_ostacoloCorretto[n_o];

				//tramite l'utilizzo dei tag cancello tutti gli oggetti in scena
				GameObject[] ogg;
				ogg = GameObject.FindGameObjectsWithTag("turbine");
				foreach (GameObject element in ogg) {
					GameObject.Destroy(element); //distruzione linee
				}
				ogg = GameObject.FindGameObjectsWithTag("ostacolo");
				foreach (GameObject element in ogg) {
					GameObject.Destroy(element); //distruzione ostacoli
				}
				canvas.SetActive(true);
				ogg = GameObject.FindGameObjectsWithTag("canvas");
				foreach (GameObject element in ogg) {
					GameObject.Destroy(element); //distruzione ostacoli
				}
				clockTimer.SetActive(true);
				ogg = GameObject.FindGameObjectsWithTag("timer");
				foreach (GameObject element in ogg) {
					GameObject.Destroy(element); //distruzione ostacoli
				}
				manager.SwitchState(new WonState(manager));
			break;
				
				
			}



			//printato lo stato corrente dopo il cambio
			if (statoApp != stato) {
				firstTime = true;
				Debug.Log ("----------------------------------------stato: " + stato);
			} else {firstTime = false;}
        }

        /// <summary>
        /// called by stateManager onGUI once per frame when state active
        /// </summary>
        /// <remarks>TO DO:
        /// if you use also this function for the logic you need to specify here what appened</remarks>
		public void ShowIt()
        {
			GUIStyle btStyleOFF = new GUIStyle (GUI.skin.button);
			btStyleOFF.normal.background = manager.gameDataRef.btOFF;
			btStyleOFF.fontSize = 14;
			btStyleOFF.normal.textColor = Color.white;
			btStyleOFF.fontStyle = FontStyle.Normal;
			btStyleOFF.alignment = TextAnchor.MiddleCenter;
			btStyleOFF.normal.textColor = Color.white;
			GUIStyle btStyleON = new GUIStyle (btStyleOFF);
			btStyleON.normal.background = manager.gameDataRef.btON;
			btStyleON.hover.background = manager.gameDataRef.btON;

             //if button pressed or R keys
			if (GUI.Button(new Rect(Screen.width - 250 - 5, 5, 250, 60), "Torna al menù",btStyleOFF) || Input.GetKeyUp(KeyCode.R))
            {	//tramite i tag distruggo tutti gli oggetti in scena
				GameObject[] ogg;
				ogg = GameObject.FindGameObjectsWithTag("Player");
				foreach (GameObject element in ogg) {
					GameObject.Destroy(element); //distruzione personaggio
				}
				ogg = GameObject.FindGameObjectsWithTag("ambiente");
				foreach (GameObject element in ogg) {
					GameObject.Destroy(element); //distruzione ambiente
				}
				ogg = GameObject.FindGameObjectsWithTag("turbine");
				foreach (GameObject element in ogg) {
					GameObject.Destroy(element); //distruzione linee
				}
				ogg = GameObject.FindGameObjectsWithTag("ostacolo");
				foreach (GameObject element in ogg) {
					GameObject.Destroy(element); //distruzione ostacoli
				}
				canvas.SetActive(true);
				ogg = GameObject.FindGameObjectsWithTag("canvas");
				foreach (GameObject element in ogg) {
					GameObject.Destroy(element); //distruzione ostacoli
				}
				clockTimer.SetActive(true);
				ogg = GameObject.FindGameObjectsWithTag("timer");
				foreach (GameObject element in ogg) {
					GameObject.Destroy(element); //distruzione ostacoli
				}
				// switch the state to Setup state
				music.Stop();
				manager.SwitchState(new SetupState(manager));
			}
			Debug.Log("In PlayState");
        }


		//metodo carica scena, crea la parte riferita alla barra di forza e al timer
		void CaricaScena(){
			canvas = GameObject.Instantiate(UnityEngine.Resources.Load("prefabs/Canvas")) as GameObject;  // find CastBar object
			forceBar = GameObject.Find ("CastBar") as GameObject;
			forceBarScript = forceBar.GetComponent<CastingBar>();   // assign CastingBar script
			clockTimer = GameObject.Instantiate(UnityEngine.Resources.Load("prefabs/alarmClockBlue")) as GameObject; // find ClockTimer object
			clockTimer.transform.position = new Vector3 (-3f, 7.6f, -4.59f);
			clockTimer.transform.eulerAngles = new Vector3(280f, -10f, 160f);
			clockTimer.transform.localScale = new Vector3(4f, 4f, 4f);
			clockScript = clockTimer.GetComponent<ClockScript>();   // assign clock timer script
			clockScript.InitArrows ();
			clockTimer.SetActive(false);
			canvas.SetActive(false);

		}
		/* //implementazione alternativa della barra
		void barraForza(float _now,float _min,float _max){
			GUIStyle boxVerde = new GUIStyle(GUI.skin.box);
			boxVerde.normal.background = manager.gameDataRef.boxVerde;
			boxVerde.hover.background = manager.gameDataRef.boxVerde;

			GUIStyle boxGiallo = new GUIStyle(GUI.skin.box);
			boxGiallo.normal.background = manager.gameDataRef.boxGiallo;
			boxGiallo.hover.background = manager.gameDataRef.boxGiallo;

			float inizH = Screen.width * 1 / 5; //punto d'inizio orizzontale
			float inizV = 10; //punto d'inizio verticale
			float largh = Screen.width*3/5;
			float sp = 30; //spessore
			_now = largh * _now;
			_min = largh * _min;
			_max = largh * _max;

			GUI.Box (new Rect(inizH,inizV,largh,sp),"",GUI.skin.box);
			GUI.Box (new Rect(inizH+_min-2,inizV-2,2,sp+4),"",GUI.skin.box);
			GUI.Box (new Rect(inizH+_max-2,inizV-2,2,sp+4),"",GUI.skin.box);

			if (_now >= _max) {
				GUI.Box (new Rect (inizH, inizV, _now, sp), "", boxGiallo);	//arancio
			} else {
				if (_now <= _min) {
					GUI.Box (new Rect (inizH, inizV, _now, sp), "", boxGiallo);	//arancio
				} else {
					GUI.Box (new Rect (inizH, inizV, _now, sp), "", boxVerde);	//verde
				}
			}
		}*/
    }
}