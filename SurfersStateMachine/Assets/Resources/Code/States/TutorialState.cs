﻿using UnityEngine;
using Assets.Resources.Code.Interfaces;
using Assets.Resources.Code.States;
using Assets.Resources.Code.Scripts;
using System.Collections.Generic;

namespace Assets.Resources.Code.States
{
    /// <summary>
    /// Play state. when you play the game you are here. in this class you manage all the logic of the game during playing
    /// </summary>
    /// <remarks>TO DO: put here a detailed description of this class and logic
    /// </remarks>
	class TutorialState : IStateBase
    {
        /// <summary>
        /// varaible to memorize the state manager object ( core of state machine )
        /// </summary>
        private StateManager manager;
        /// <summary>
        /// raference to the shooter object
        /// </summary>
        //private GameObject shooter;
        /// <summary>
        /// memorize here the script attached to the shooter object
        /// </summary>
        //public Shooter shooterScript;
        /// <summary>
        /// reference to the force bar object
        /// </summary>
        private GameObject forceBar;
        /// <summary>
        /// memorize here the script attached to the force bar object
        /// </summary>
        public CastingBar forceBarScript;
        /// <summary>
        /// reference to the countdown timer object
        /// </summary>
        private GameObject clockTimer;
        /// <summary>
        /// memorize here the script attached the timer
        /// </summary>
        public ClockScript clockScript;
		private GameObject canvas;




		// parametri
		public static int n_num = 6;
		int n_o;
		int tipi_oggetto = 6;
		float soglia = 0.5f; // soglia radianti tra rette
		float rapp = (float)(n_num + 1); //indica la frazione della diagonale d'ambiente che da la vicinanza max tra oggetti
		float v_linea = 3f;
		float cornice = 0.65f;
		float t_stupido;
		float sei_stupido = 10f;
		float attesa_start = 5f;
		float attesa_jump = 0.1f;
		float attesa_fase2 = 10f;
		float attesa_risultati = 8f;
		float soglia_pick = 2f;
		float fade_ostacoli =  3f;
		
		float force_limit1 = 1f;
		float force_limit2 = 2f;
		float force_limit3 = 3f;
		float force_limit4 = 4f;
		
		Vector3 sut = new Vector3 (0f, 0.2f, 0f);
		Vector3 startc_p = new Vector3 (-0.38f, 10.23f, -9.07f); //(0f, 11.1f, -10.6f);
		Vector3 stopc_p = new Vector3 (6.55f, 1.38f, 2.51f);
		Vector3 startc_r = new Vector3 (52f, 0f, 0f);
		Vector3 stopc_r = Vector3.zero;
		int conttel = 0;
		float Tcam = 4f;
		
		GameObject ambiente;
		Ostacolo[] oggetti = new Ostacolo[n_num]; 
		Vector3 tVector1,tVector2,tVector3,p;
		
		LineRenderer[] linee = new LineRenderer[n_num + 1];
		GameObject[] lineeg = new GameObject[n_num + 1];
		float[] track = new float[n_num + 1];
		Vector3[] posizione = new Vector3[n_num + 2];
		float[] saveOggetti = new float[n_num];
		
		float rx,rz,ang1,ang2,ang3,dist,counter,t,ssx,ssy,ssz,max_forza;
		public float forza = 0;
		
		float[] r_circT = {1.7f,1.55f,1.5f,0.8f,0.6f,0.6f};
		float[,] forzeT = new float[8,2];
	//moto; panchina; tronchi; birilli?; basket; ??;
		float[] altezzeT = {0.8f,1f,0.6f,0.5f,0.7f,0.4f};
		//moto 0.8; panchina 1; tronchi 0.6; birilli 0.5; 
		int[] tipo_saltoT = {1,1,1,0,0,0};
				
	int cont,flag,tOggetto;
	int stato=0;
	bool firstTime=true;
	bool inizioPressione = false;
	float actualTime,actualForce;

		
		bool c1m,c2m;
				
		Personaggio boy;
		
		bool girotondoInEsecuzione,raggiungiInEsecuzione,SaltoInEsecuzione;
		
		Camera myCamera;
		float[] parFloat;
		bool[] parBool;

		Vector3 initialPos;

		float tempoOstacoli;
		bool lanciatoSuOstacolo=false;

		// per risultati
		Vector3[] r_startPos = new Vector3[n_num + 1];

		Vector3[] r_endPos = new Vector3[n_num + 1];
		float[] r_spacePC = new float[n_num + 1];
		float[] r_spaceHU = new float[n_num + 1];
		Vector3 lastPos;

		float[] r_startTimePC = new float[n_num + 1];
		float[] r_endTimePC = new float[n_num + 1];
		float[] r_startTimeHU = new float[n_num + 1];
		float[] r_endTimeHU = new float[n_num + 1];

		bool r_primoGiusto = false;
		bool r_ultimoGiusto = false;
		bool[] r_ostacoloCorretto = new bool[n_num + 1];
		float r_rilasciTrascinamento = 0f;
		float r_rilasciSalto = 0f;


        /// <summary>
        /// Create the state and load the Scene01
        /// </summary>
        /// <param name="managerRef"> store the state manager object reference</param>
        /// <remarks>when initialize play state we also take the reference to the player object and to 
        /// objects for the force management ( force bar and clock timer )</remarks>
        public TutorialState(StateManager managerRef)    // constructor
        {
            manager = managerRef;   // store a reference to StateManager class

			CaricaScena ();

			myCamera = Camera.main;
			myCamera.clearFlags = CameraClearFlags.Skybox;


			n_o = 1;
			forzeT[0,0] = manager.gameDataRef.parFloat [4];
			forzeT[1,0] = manager.gameDataRef.parFloat [4];
			forzeT[2,0] = manager.gameDataRef.parFloat [4];
			forzeT[3,0] = manager.gameDataRef.parFloat [2];
			forzeT[4,0] = manager.gameDataRef.parFloat [2];
			forzeT[5,0] = manager.gameDataRef.parFloat [2];
			
			forzeT[0,1] = manager.gameDataRef.parFloat [5];
			forzeT[1,1] = manager.gameDataRef.parFloat [5];
			forzeT[2,1] = manager.gameDataRef.parFloat [5];
			forzeT[3,1] = manager.gameDataRef.parFloat [3];
			forzeT[4,1] = manager.gameDataRef.parFloat [3];
			forzeT[5,1] = manager.gameDataRef.parFloat [3];

			tempoOstacoli = 3f;

			// aiuto: basta premere su ostacolo.




		myCamera.transform.position = startc_p; //per visione 3d
			//transform.position = new Vector3 (-0.1f, 9.6f, -0.1f); //per visione alta
			myCamera.transform.eulerAngles = startc_r;// per visione 3d
			//transform.eulerAngles = new Vector3 (90f, 0f, 0f);/per visione alta
		
			// inizializzo ambiente

			ambiente = GameObject.Instantiate (UnityEngine.Resources.Load("ambiente")) as GameObject;
			
		ambiente.transform.position = new Vector3 (0f, 0f, 0f);
			ambiente.transform.eulerAngles = new Vector3 (0f, 0f, 0f);

		posizione[0] = ambiente.transform.GetChild (0).position;
			posizione[n_o+1] = ambiente.transform.GetChild (1).position;
			
			//dist = Mathf.Sqrt(((posizione[0].x - posizione[n_o+1].x)*(posizione[0].z - posizione[n_o+1].z)/(float)rapp));
			dist = Mathf.Max (Vector3.Distance(posizione[0],posizione[n_o+1])/rapp,2f*Mathf.Max(r_circT));
			
			parFloat = manager.gameDataRef.parFloat;
			parBool = manager.gameDataRef.parBool;

			//ASSEGNAZIONE OGGETTI
			//assegnazione primo oggetto
			flag = 0;
			while (flag == 0) {
				rx  = 0f;
				rz  = 0f;
				if (true) {
					flag = 1; } 
			}
			
			posizione [1] = new Vector3 (rx , 0f, rz );
			tVector1 = posizione[1] - posizione[0];

			//scelta oggetto
			tOggetto = 0;
			saveOggetti [0] = tOggetto;
			
			//creazione oggetto
			oggetti [0] = new Ostacolo (posizione [1], new Vector3 (0f, Mathf.Rad2Deg * (-Mathf.Atan2 (tVector1.z, tVector1.x)), 0f), "Oggetto" + (tOggetto + 1), r_circT [tOggetto], forzeT [tOggetto,0], forzeT [tOggetto,1],altezzeT [tOggetto],tipo_saltoT[tOggetto]);
			
			//posiziono personaggio
			boy = new Personaggio (posizione[0],posizione[1]);
			r_startPos[0] = posizione[0];


		//assegnazione altri oggetti
			for (int i=2; i < n_o+1; i++) {
				flag = 0;
				float tempo_c = Time.time;
				while (flag == 0) {
					if ((Time.time-tempo_c) > 1.5f) {
						flag = 0;
						while (flag == 0) {
							rx  = Random.Range (posizione[0].x+cornice, posizione[n_o+1].x-cornice);
							rz  = Random.Range (posizione[0].z+cornice, posizione[n_o+1].z-cornice);
							if ((Vector3.Distance(new Vector3(rx,0f,rz),posizione[0]) > 2f*dist) && (Vector3.Distance(new Vector3(rx,0f,rz),posizione[n_o+1]) > dist)) {
								flag = 1; } 
						}
						
						posizione [1] = new Vector3 (rx , 0f, rz );
						tVector1 = posizione[1] - posizione[0];
						
						//scelta oggetto
						tOggetto = Random.Range (0, tipi_oggetto);
						saveOggetti [0] = tOggetto;
						
						//creazione oggetto
						oggetti [0] = new Ostacolo (posizione [1], new Vector3 (0f, Mathf.Rad2Deg * (-Mathf.Atan2 (tVector1.z, tVector1.x)), 0f), "Oggetto" + (tOggetto + 1), r_circT [tOggetto], forzeT [tOggetto,0], forzeT [tOggetto,1],altezzeT [tOggetto],tipo_saltoT[tOggetto]);

						i = 2;
					}
					flag = 1;
					rx = Random.Range (posizione [0].x + cornice, posizione [n_o + 1].x - cornice);
					rz = Random.Range (posizione [0].z + cornice, posizione [n_o + 1].z - cornice);
					posizione [i] = new Vector3 (rx, 0f, rz);
					//controllo distanze
					for (int j = 0; j < i-1; j++) {
						//distanza da oggetti
						if (Vector3.Distance (new Vector3 (rx, 0f, rz), posizione [j]) < dist) {
							flag = 0;
						}
						//distanza da oggetto precedente
						if (Vector3.Distance (new Vector3 (rx, 0f, rz), posizione [i - 1]) < 2f * dist) {
							flag = 0;
						}
						//distanza da traguardo finale
						if (Vector3.Distance (new Vector3 (rx, 0f, rz), posizione [n_o + 1]) < dist) {
							flag = 0;
						}
					}
					
					//controllo posizioni proibite
					if (flag == 1) {
						//controllo conflitti con oggetti già esistenti
						for (int j=0; j<i-1; j++) {
							//vettore oggetto -> altro_oggetto
							tVector1 = posizione [j] - posizione [i];
							ang1 = Mathf.Atan2 (tVector1.z, tVector1.x);
							ang1 = (ang1 + Mathf.PI * 2f) % (Mathf.PI * 2f);
							//vettore oggetto -> altro_oggetto+1
							tVector2 = posizione [j + 1] - posizione [i];
							ang2 = Mathf.Atan2 (tVector2.z, tVector2.x);
							ang2 = (ang2 + Mathf.PI * 2f) % (Mathf.PI * 2f);
							//vettore oggetto-1 -> altro_oggetto
							tVector3 = posizione [j] - posizione [i - 1];
							ang3 = Mathf.Atan2 (tVector3.z, tVector3.x);
							ang3 = (ang3 + Mathf.PI * 2f) % (Mathf.PI * 2f);

							//controllo di soglia sulla condizione1
							c1m = Mathf.Abs (Mathf.Abs (ang1 - ang2) - Mathf.PI) < soglia;
							
							//controllo di soglia sulla condizione1
							c2m = Mathf.Abs (Mathf.Abs ((ang1 + Mathf.PI) % (Mathf.PI * 2f) - (ang3 + Mathf.PI) % (Mathf.PI * 2f)) - Mathf.PI) < soglia;
							
							// accetto la posizione per l'altro oggetto j-esimo
							if (c1m || c2m) {
								flag = 0;
							}
							
							//se è l'ultimo oggetto va controllata anche posizione rispetto a traguardo
							if (i == n_o && flag == 1) {
								//vettore altro_oggetto+1 -> traguardo finale
								tVector1 = posizione [n_o + 1] - posizione [j + 1];
								ang1 = Mathf.Atan2 (tVector1.z, tVector1.x);
								ang1 = (ang1 + Mathf.PI * 2f) % (Mathf.PI * 2f);
								c1m = Mathf.Abs (Mathf.Abs (ang1 - (ang2 + Mathf.PI) % (Mathf.PI * 2f)) - Mathf.PI) < soglia;
								//Debug.Log ("ang1" + ang1 + "j" + j);
								if (c1m) {
									flag = 0;
								}
							}
						}
					}
				}		

				//scelgo tipo d'oggetto e creo il nuovo ostacolo
				tVector1 = posizione [i] - posizione [i - 1];
				
				//scelta oggetto
				if(true){
					tOggetto = Random.Range (0, tipi_oggetto); //ostacoli anche uguali
				} else{ //ostacoli sempre diversi
					bool notGood = true;
					while (notGood){
						tOggetto = Random.Range (0, tipi_oggetto);
						notGood=false;
						for (int j=0;j<i-1;j++){
							if(tOggetto==saveOggetti[j]){
								notGood=true;
							}
						}
					}
					saveOggetti[i-1] = tOggetto;
				}
				
				//creazione oggetto
				oggetti [i - 1] = new Ostacolo (posizione [i], new Vector3 (0f, Mathf.Rad2Deg * (-Mathf.Atan2 (tVector1.z, tVector1.x)), 0f), "Oggetto" + (tOggetto + 1), r_circT [tOggetto], forzeT [tOggetto, 0], forzeT [tOggetto, 1], altezzeT [tOggetto], tipo_saltoT [tOggetto]);
			}
				//inizializzo linee
				for (int i=0; i<n_o+1; i++) {
					lineeg[i] = GameObject.Instantiate (UnityEngine.Resources.Load ("linea")) as GameObject;
					lineeg[i].tag = "turbine";
					linee[i] = lineeg[i].GetComponent<LineRenderer>();
					linee[i].SetWidth(0.1f,0.1f);
				}
				
				track[0] = Vector3.Distance(posizione[0],posizione[1]);
				linee[0].SetPosition(0,posizione[0]+sut);
				linee[0].SetPosition(1,posizione[0]);
				for (int i=1; i<n_o+1; i++) {
					//Debug.Log(linee[i]);
					linee[i].SetWidth(0.1f,0.1f);
					track[i] = Vector3.Distance(posizione[i],posizione[i+1]);
					linee[i].SetPosition(0,(posizione[i+1]-posizione[i])*(oggetti[i-1].r_circ/track[i])+posizione[i]+sut);
					linee[i].SetPosition(1,(posizione[i+1]-posizione[i])*(oggetti[i-1].r_circ/track[i])+posizione[i]+sut);
				}
				
				cont = 0;
				counter = 0;
				stato = 0;
		
			for (int h=0; h<n_num; h++) {
				r_ostacoloCorretto[h] = true;
			}

	}

        /// Update is called by stateManager once per frame when state active
        public void StateUpdate()
		{	

            // if mouse is present update the force value
			if (Input.mousePresent && ( stato == 8 || stato==10 || stato==20)) {
				// if Arduino connected simulate force with it
				if (false){//manager.sp.IsOpen) {                
					// pass to the function the bool value to simulate decrease and increase of force
					manager.gameDataRef.forceManager.UpdateForce (manager.gameDataRef.ArduinoButtonA, manager.gameDataRef.ArduinoButtonB,manager.gameDataRef.parBool[5]);
					actualForce = manager.gameDataRef.forceManager.actualForce / 1000;
				}
				// pass to the function the bool value to simulate decrease and increase of force using keyboard
				else {
					manager.gameDataRef.forceManager.UpdateForce (Input.GetKey (KeyCode.A), Input.GetKey (KeyCode.B),manager.gameDataRef.parBool[5]);
					actualForce = manager.gameDataRef.forceManager.actualForce / 1000;
				}
				//if timer is active update it
				if (manager.gameDataRef.forceManager.IsTimerActive) {
					float actualRemainingTime = manager.gameDataRef.forceManager.remainingTime;
					manager.gameDataRef.forceManager.updateTimer();
					clockScript.UpdateClock ((manager.gameDataRef.forceManager.remainingTime - actualRemainingTime) * 360 / 5f);//manager.gameDataRef.forceManager.RemainingTimeSetted);
				}
				//else if I'm in threshold and at costant value start it
				else if (manager.gameDataRef.forceManager.InThresholdValue == ForceManager.InThreshold.inner && manager.gameDataRef.forceManager.InStdDevForveLimit && stato==10) {
					manager.gameDataRef.forceManager.StartTimer ();
					clockTimer.SetActive (true);
				}
				forceBarScript.UpdateCastBar (manager.gameDataRef.forceManager.actualForce); // set the actual force value in the bar
				forceBarScript.UpdateCastBarColor ((int)manager.gameDataRef.forceManager.InThresholdValue); // update force bar color
			} else {
				//canvas.SetActive(false);
			}       
			
			// if timer is inactive hide the clock timer and reset it
			if (!manager.gameDataRef.forceManager.IsTimerActive) {
				//GameObject[] ogg = GameObject.FindGameObjectsWithTag("timer");
				//foreach (GameObject element in ogg) {
				//	element.SetActive(false); //distruzione ostacoli
				//}
				clockScript.restArrow();
				clockTimer.SetActive (false);

			} else {

			}





			int statoApp = stato;
			switch(stato) { //macchina a stati
				
			case 0: //personaggio fa riscaldamento per qualche secondo
				boy.ANIM_risc = true;
			if(firstTime) { 
				actualTime = Time.time;
			}
			if(Time.time > actualTime+attesa_start) {
				boy.ANIM_risc = false;
				stato = 1;
			}

				break;
				
			case 1: //personaggio raggiunge prossimo ostacolo durante FASE 1
				if (tempoOstacoli<0.1f) {tempoOstacoli = Random.Range(2,5);}


				if (cont < n_o) { //se non deve raggiungere l'arrivo
					oggetti[cont].changecolor(true);
					oggetti[cont].psyco(Time.time,2f);
					if(firstTime) { 
						r_startTimePC[cont] = Time.time;
						initialPos = boy.myPersonaggio.transform.position;
						if (tempoOstacoli<0.1f) {
							v_linea = 1.5f;
						} else {
							v_linea = (Vector3.Distance(posizione[cont+1],initialPos)-oggetti[cont].r_circ)/tempoOstacoli/0.92f;
						}
						boy.Raggiungi(posizione[cont+1],v_linea,oggetti[cont].r_circ);
					}
					
					if (counter < track[cont]-oggetti[cont].r_circ) { //disegna linea solo all'esterno delle circonferenza
						counter = Vector3.Magnitude(boy.myPersonaggio.transform.position - posizione[cont]) ;
						linee [cont].SetPosition (1,boy.myPersonaggio.transform.position + sut);
					}
					if (boy.Raggiungi(posizione[cont+1],v_linea,oggetti[cont].r_circ)) { // quando è arrivato nei pressi dell'ostacolo
						r_endPos[cont] = boy.myPersonaggio.transform.position;
						r_endTimePC[cont] = Time.time;
						counter = 0f;
						stato = 2;
					}					
				} else { //deve raggiungere l'arrivo
					if(firstTime) { 
						r_startTimePC[cont] = Time.time;
						initialPos = boy.myPersonaggio.transform.position;
						if (tempoOstacoli<0.1f) {
							v_linea = 1.5f;
						} else {
							v_linea = (Vector3.Distance(posizione[cont+1],initialPos)-0.1f)/tempoOstacoli/0.91f;
						}
						boy.Raggiungi(posizione[cont+1],v_linea,0.1f);
						}
					
					if (counter < track[cont]) { //disegna linea fino in fondo
						counter = Vector3.Magnitude(boy.myPersonaggio.transform.position - posizione[cont]) ;
						linee [cont].SetPosition (1,boy.myPersonaggio.transform.position + sut);
					}
					if (boy.Raggiungi()) {//quando è arrivato all'arrivo
						r_endPos[cont] = boy.myPersonaggio.transform.position;
						r_endTimePC[cont] = Time.time;
						stato = 5;
					}
				}
				break;
				
			case 2: //piccola pausa mentre il personaggio carica il salto durante FASE 1
				boy.ANIM_carSalto = true;
				if(firstTime) { 
					actualTime = Time.time;
				}
			if(Time.time > actualTime+attesa_jump) {
				boy.ANIM_carSalto = false;
				stato = 3;
			}
				break;
				
			case 3: //personaggio salta in automatico durante FASE 1
				if(firstTime) { boy.Salta(oggetti[cont].altezza,oggetti[cont].r_circ*2f,oggetti[cont].salto); }
				if (boy.Salta ()) { //quando salto è finito
					stato = 4;
				}
				break;
				
			case 4: //personaggio fa il girotondo all'ostacolo appena saltato durante FASE 1
				if (firstTime) { 
					r_startPos[cont+1] = boy.myPersonaggio.transform.position;
					boy.Girotondo(posizione[cont+1],posizione[cont+2],true); }
				if (boy.Girotondo()) { //quando girotondo è finito
					oggetti[cont].changecolor(false);
					cont = cont + 1;
					stato = 1;
				}
				break;
				
			case 5: //personaggio esulta
			boy.ANIM_fest = true;
			canvas.SetActive(true);

				if (firstTime) {boy.Guarda(myCamera.transform.position); }
				if(firstTime) { 
					actualTime = Time.time;
				}
			if(Time.time > actualTime+attesa_fase2) {
				boy.ANIM_fest = false;
				stato = 6;}
				break;
				
			case 6: //si aspetta che l'utente prema una cosa tipo "start"
				if (true){
					for (int i=0; i<n_o+1; i++) {
						GameObject.Destroy(lineeg [i]); //distruzione linee
					}
				}
				GameObject.Destroy(boy.myPersonaggio); //distruzione personaggio
				boy = new Personaggio (posizione[0],posizione[1]); //creazione nuovo personaggio
				boy.ANIM_risc = true;
				cont = 0;
				stato = 8;
				r_startTimeHU[0] = Time.time;

				break;
				
			case 7: //inutile. by PAolo
				break;
				
				//FASE 2 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! FASE 2 !!!!!!!
			case 8: //personaggio segue il mouse fino al prossimo ostacolo. 
				Debug.Log("qua");
				if(firstTime){
					manager.gameDataRef.forceManager.ForceLowerLimit = manager.gameDataRef.parFloat[0]*1000;
					manager.gameDataRef.forceManager.ForceUpperLimit = manager.gameDataRef.parFloat[1]*1000;
					forceBarScript.InitializeForceBar(manager.gameDataRef.forceManager.ForceLowerLimit, manager.gameDataRef.forceManager.ForceUpperLimit, manager.gameDataRef.forceManager.actualForce);    // initialize force bar value
					t_stupido = Time.time;
					lanciatoSuOstacolo=false;
					lastPos = r_startPos[cont];
				}

				if (Time.time-t_stupido > sei_stupido && cont < n_o) {
					oggetti[cont].changecolor(true);
					oggetti[cont].psyco(Time.time,2f);
				}





				if (manager.gameDataRef.forceManager.InThresholdValue == ForceManager.InThreshold.inner) { //forza sopra prima soglia
					Debug.Log("qui");
					boy.ANIM_risc = false;
					ssx = myCamera.ScreenPointToRay(Input.mousePosition).direction.x;
					ssy = myCamera.ScreenPointToRay(Input.mousePosition).direction.y;
					ssz = myCamera.ScreenPointToRay(Input.mousePosition).direction.z;
					t = -myCamera.transform.position.y/ssy;
					p = new Vector3(myCamera.transform.position.x + t*ssx,0f,myCamera.transform.position.z + t*ssz);
					
					// se l'utente ha premuto abbastanza vicino
					float distance = Vector3.Magnitude(p-boy.myPersonaggio.transform.position);
					if (false){
						lanciatoSuOstacolo = true;
						soglia_pick = 200f;
						distance = 1f;
					}
					if (distance < soglia_pick) {
						r_spaceHU[cont] += Vector3.Distance(lastPos,boy.myPersonaggio.transform.position);
						lastPos = boy.myPersonaggio.transform.position;
						boy.Raggiungi(p,5f*distance,0.1f);
					} else { // l'utente ha premuto troppo lontano
						boy.Stop(); //stop
					}

				} else { // l'utente non ha premuto sullo schermo
					if(lanciatoSuOstacolo){
						r_spaceHU[cont] += Vector3.Distance(lastPos,boy.myPersonaggio.transform.position);
						lastPos = boy.myPersonaggio.transform.position;
						boy.Raggiungi();
					} else {
					boy.Stop(); //stop
					}
				}


				//valuta se ha sbagliato l'ostacolo
				for (int h=0; h<cont; h++){ //ne ha preso uno precedente
					if (Vector3.Distance(p,posizione[h])<(oggetti[h].r_circ/2f)){
						r_ostacoloCorretto[cont] = false;
						//Debug.Log ("tratta: "+cont+" errori: "+r_ostacoloCorretto[cont]);
					}
				}
				for (int h=cont+1; h<n_o; h++){ //ne ha preso uno successivo
					if (Vector3.Distance(p,posizione[h+1])<(oggetti[h].r_circ/2f)){
						r_ostacoloCorretto[cont] = false;
						//Debug.Log ("tratta: "+cont+" errori: "+r_ostacoloCorretto[cont]);
					}
				}

				// gestione dello stop
				if (cont < n_o) { // se non deve raggiungere l'arrivo
					if (Vector3.Magnitude(posizione[cont+1]-boy.myPersonaggio.transform.position) < oggetti[cont].r_circ) {
						oggetti[cont].changecolor(false);
						r_endTimeHU[cont] = Time.time;
						boy.Stop(); //stop
						boy.Girotondo(posizione[cont+1],posizione[cont],false);
						cont = cont + 1; 
						stato = 9;
					}
				} else { // se deve raggiungere l'arrivo
					if (Vector3.Magnitude(posizione[cont+1]-boy.myPersonaggio.transform.position) < 0.3f) {
						r_endTimeHU[cont] = Time.time;
						boy.Stop();
						conttel = 0;
						stato = 15;
					}
				}
				break;
				
			case 9: //personaggio si riallinea prima di saltare
				if(boy.Girotondo()) {
					max_forza = 0;
					forza = 0;
					stato = 10;
				}
				break;
				
		case 10:
			//boy.ANIM_carSalto = true;
			//Debug.Log(actualForce);
			if(firstTime){
					manager.gameDataRef.forceManager.ForceLowerLimit = oggetti[cont-1].forza*1000;//minimo
					manager.gameDataRef.forceManager.ForceUpperLimit = oggetti[cont-1].forzaLim*1000;//massimo
					forceBarScript.InitializeForceBar(manager.gameDataRef.forceManager.ForceLowerLimit, manager.gameDataRef.forceManager.ForceUpperLimit, manager.gameDataRef.forceManager.actualForce);    // initialize force bar value
				}
			//float val = Mathf.Min(1f,actualForce/oggetti[cont-1].forza);
			//boy.Carica(val);
				if (manager.gameDataRef.forceManager.remainingTime<0.15f && manager.gameDataRef.forceManager.remainingTime>0.01f) { //aggiorna massima forza applicata
					boy.ANIM_carSalto = false;
					stato = 13;
			}
			
			break;
		
		case 11:
			break;
				
			case 12:
			break;
				
			case 13: //premuto ok, personaggio salta e supera l'ostacolo
				if (firstTime) { boy.Salta(oggetti[cont-1].altezza,oggetti[cont-1].r_circ*2f,oggetti[cont-1].salto); }
				if (boy.Salta()) {
					if (true){
						oggetti[cont-1].distruggi(fade_ostacoli);
					}

					stato = 14;
				}
				break;
				
			case 14: //utile grazieeee
				if (oggetti[cont-1].vuoto == false && true) {
					oggetti[cont-1].distruggi(fade_ostacoli);
				}
				else {
					r_startTimeHU[cont] = Time.time;
					stato = 20;
				}
				break;

			case 20:
				if(firstTime){
					manager.gameDataRef.forceManager.ForceLowerLimit = manager.gameDataRef.parFloat[0]*1000;
					manager.gameDataRef.forceManager.ForceUpperLimit = manager.gameDataRef.parFloat[1]*1000;
					forceBarScript.InitializeForceBar(manager.gameDataRef.forceManager.ForceLowerLimit, manager.gameDataRef.forceManager.ForceUpperLimit, manager.gameDataRef.forceManager.actualForce);    // initialize force bar value
					t_stupido = Time.time;
					lanciatoSuOstacolo=false;
					lastPos = r_startPos[cont];
				}
				
				if (Time.time-t_stupido > sei_stupido && cont < n_o) {
					oggetti[cont].changecolor(true);
					oggetti[cont].psyco(Time.time,2f);
				}
				// conto i rilasci durante il trascinamento
				if (manager.gameDataRef.forceManager.InThresholdValue == ForceManager.InThreshold.under) {
					r_rilasciTrascinamento += 1f;
					Debug.Log("rilasci: "+r_rilasciTrascinamento);
				}
				
				
				
				
				if (manager.gameDataRef.forceManager.InThresholdValue == ForceManager.InThreshold.inner) { //forza sopra prima soglia
					boy.ANIM_risc = false;
					ssx = myCamera.ScreenPointToRay(Input.mousePosition).direction.x;
					ssy = myCamera.ScreenPointToRay(Input.mousePosition).direction.y;
					ssz = myCamera.ScreenPointToRay(Input.mousePosition).direction.z;
					t = -myCamera.transform.position.y/ssy;
					p = new Vector3(myCamera.transform.position.x + t*ssx,0f,myCamera.transform.position.z + t*ssz);
					
					// se l'utente ha premuto abbastanza vicino
					float distance = Vector3.Magnitude(p-boy.myPersonaggio.transform.position);
					if (false){
						lanciatoSuOstacolo = true;
						soglia_pick = 200f;
						distance = 1f;
					}
					if (distance < soglia_pick) {
						r_spaceHU[cont] += Vector3.Distance(lastPos,boy.myPersonaggio.transform.position);
						lastPos = boy.myPersonaggio.transform.position;
						boy.Raggiungi(p,5f*distance,0.1f);
					} else { // l'utente ha premuto troppo lontano
						boy.Stop(); //stop
					}
					
				} else { // l'utente non ha premuto sullo schermo
					if(lanciatoSuOstacolo){
						r_spaceHU[cont] += Vector3.Distance(lastPos,boy.myPersonaggio.transform.position);
						lastPos = boy.myPersonaggio.transform.position;
						boy.Raggiungi();
					} else {
						boy.Stop(); //stop
					}
				}
				
				
				//valuta se ha sbagliato l'ostacolo
				for (int h=0; h<cont; h++){ //ne ha preso uno precedente
					if (Vector3.Distance(p,posizione[h])<(oggetti[h].r_circ/2f)){
						r_ostacoloCorretto[cont] = false;
						//Debug.Log ("tratta: "+cont+" errori: "+r_ostacoloCorretto[cont]);
					}
				}
				for (int h=cont+1; h<n_o; h++){ //ne ha preso uno successivo
					if (Vector3.Distance(p,posizione[h+1])<(oggetti[h].r_circ/2f)){
						r_ostacoloCorretto[cont] = false;
						//Debug.Log ("tratta: "+cont+" errori: "+r_ostacoloCorretto[cont]);
					}
				}
				
				// gestione dello stop
				if (cont < n_o) { // se non deve raggiungere l'arrivo
					if (Vector3.Magnitude(posizione[cont+1]-boy.myPersonaggio.transform.position) < oggetti[cont].r_circ) {
						oggetti[cont].changecolor(false);
						r_endTimeHU[cont] = Time.time;
						boy.Stop(); //stop
						boy.Girotondo(posizione[cont+1],posizione[cont],false);
						cont = cont + 1; 
						stato = 9;
					}
				} else { // se deve raggiungere l'arrivo
					if (Vector3.Magnitude(posizione[cont+1]-boy.myPersonaggio.transform.position) < 0.3f) {
						r_endTimeHU[cont] = Time.time;
						boy.Stop();
						conttel = 0;
						stato = 15;
					}
				}

				break;

				
			case 15: // finito
				canvas.SetActive(false);
				boy.ANIM_fest = true;
				
				if(firstTime) { 
					Debug.Log ("FINITO TUTTO YEEE!");
				}
				if (Vector3.Distance (myCamera.transform.position, stopc_p) > 0.001f) {
					myCamera.transform.position = new Vector3(Mathf.Lerp(startc_p.x,stopc_p.x,conttel*(0.03f/Tcam)),Mathf.Lerp(startc_p.y,stopc_p.y,conttel*(0.03f/Tcam)),Mathf.Lerp(startc_p.z,stopc_p.z,conttel*(0.03f/Tcam)));
					myCamera.transform.eulerAngles = new Vector3 (Mathf.Lerp(startc_r.x,stopc_r.x,conttel*(0.03f/Tcam)),Mathf.Lerp(startc_r.y,stopc_r.y,conttel*(0.03f/Tcam)),Mathf.Lerp(startc_r.z,stopc_r.z,conttel*(0.03f/Tcam)));
					
					boy.Guarda(Camera.main.transform.position);
					conttel += 1;
				}
				break;
		case 16: 
			// calcolo dei risultati
			break;
				
				
			}




			if (statoApp != stato) {
				firstTime = true;
				Debug.Log ("----------------------------------------stato: " + stato);
			} else {firstTime = false;}
        }

        /// <summary>
        /// called by stateManager onGUI once per frame when state active
        /// </summary>
        /// <remarks>TO DO:
        /// if you use also this function for the logic you need to specify here what appened</remarks>
		public void ShowIt()
        {

			GUIStyle boxStyleTut = new GUIStyle(GUI.skin.box);
			boxStyleTut.fontSize = 16;
			boxStyleTut.normal.textColor = Color.white;
			boxStyleTut.fontStyle = FontStyle.BoldAndItalic;

			GUIStyle btStyleOFF = new GUIStyle(GUI.skin.button);
			btStyleOFF.normal.background = manager.gameDataRef.btOFF;
			btStyleOFF.fontSize = 14;
			btStyleOFF.normal.textColor = Color.white;
			btStyleOFF.fontStyle = FontStyle.Normal;
			btStyleOFF.alignment = TextAnchor.MiddleCenter;
			btStyleOFF.normal.textColor = Color.white;

			GUIStyle labelStyle2 = new GUIStyle();
			labelStyle2.fontSize = 16;
			labelStyle2.normal.textColor = Color.white;
			labelStyle2.fontStyle = FontStyle.BoldAndItalic;
			labelStyle2.alignment = TextAnchor.MiddleLeft;
			labelStyle2.padding = new RectOffset (10, 0, 0, 0);
			
			GUIStyle labelStyle3 = new GUIStyle(labelStyle2);
			labelStyle3.fontSize = 15;
			//labelStyle3.normal.textColor = Color.white;
			labelStyle3.fontStyle = FontStyle.Normal;
			labelStyle3.alignment = TextAnchor.UpperLeft;
			labelStyle3.padding = new RectOffset (10, 0, 0, 0);
			labelStyle3.wordWrap = true;

			GUI.BeginGroup (new Rect (Screen.width / 2, Screen.height / 2 + 50, Screen.width / 2-50, Screen.height / 2-100),"Segui le indicazioni:", boxStyleTut);
			Rect posiz = new Rect (5, 40, Screen.width / 2 - 60, Screen.height / 2 - 150);
			if (stato<5) {
				GUI.Label(posiz,"In questa fase il pc mostra il percorso da " +
					"seguire, il personaggio raggiunge l'ostacolo " +
					"e lo salta dirigendosi all'arrivo.",labelStyle3);			
			}
			if (stato==5) {
				GUI.Label(posiz,"Ora è il tuo turno, il personaggio viene " +
				        "riposizionato e sarai tu a muoverlo " +
						"ripercorrendo il percorso appena visto.\n\n" +
				        "NOTA: In alto è comparsa la barra che " +
				        "indica la pressione sullo schermo." ,labelStyle3);			
			}
			if (stato==8) {
				GUI.Label(posiz,"Premi vicino al personaggio con una " +
				    "forza compresa tra le due linee, la barra " +
				    "si colorerà di verde. " +
				    "Quindi trascina lentamente il personaggio " +
				    "fino a raggiungere l'ostacolo.\n\n" +
				    "NOTA: fai attenzione a non premere troppo " +
				    "lontano dal personaggio, cerca di trascinarlo.\n\n" +
				    "VAI!",labelStyle3);			
			}
			if (stato==10) {
				GUI.Label(posiz,"Ora il Personaggio deve riuscire " +
					"a saltare l'ostacolo!\nPremi sullo schermo " +
					"cercando di mantenere verde il colore della barra " +
					"fintanto che l'orologio non esegue un giro.\n\n" +
					"NOTA: durante il gioco ci saranno ostacoli " +
					"alti o bassi, fai attenzione ai limiti sulla barra.",labelStyle3);			
			}
			if (stato==13 || stato==14) {
				GUI.Label(posiz,"Perfetto!\n" +
					"Ora l'ostacolo è alle spalle e viene eliminato.",labelStyle3);			
			}
			if (stato==20 ) {
				GUI.Label(posiz,"Come per il primo trascinamento, raggiungi " +
				    "il bollino rosso rappresentate l'arrivo.\n\n" +
				    "NOTA: fai attenzione a non premere troppo " +
				    "lontano dal personaggio, cerca di trascinarlo.",labelStyle3);			
			}
			if (stato==15 || stato==16 ) {
				GUI.Label(posiz,"\nArrivati!!!!\n" +
					"Il tutorial è finito, sei pronto per giocare!!",labelStyle3);			
			}


			GUI.EndGroup ();



             //if button pressed or R keys
			if (GUI.Button(new Rect(Screen.width - 250 - 5, 40, 250, 60), "Torna alle istruzioni",btStyleOFF) || Input.GetKeyUp(KeyCode.R))
            {
				GameObject[] ogg;
				ogg = GameObject.FindGameObjectsWithTag("Player");
				foreach (GameObject element in ogg) {
					GameObject.Destroy(element); //distruzione personaggio
				}
				ogg = GameObject.FindGameObjectsWithTag("ambiente");
				foreach (GameObject element in ogg) {
					GameObject.Destroy(element); //distruzione ambiente
				}
				ogg = GameObject.FindGameObjectsWithTag("turbine");
				foreach (GameObject element in ogg) {
					GameObject.Destroy(element); //distruzione linee
				}
				ogg = GameObject.FindGameObjectsWithTag("ostacolo");
				foreach (GameObject element in ogg) {
					GameObject.Destroy(element); //distruzione ostacoli
				}
				canvas.SetActive(true);
				ogg = GameObject.FindGameObjectsWithTag("canvas");
				foreach (GameObject element in ogg) {
					GameObject.Destroy(element); //distruzione ostacoli
				}
				clockTimer.SetActive(true);
				ogg = GameObject.FindGameObjectsWithTag("timer");
				foreach (GameObject element in ogg) {
					GameObject.Destroy(element); //distruzione ostacoli
				}
				// switch the state to Setup state
				manager.SwitchState(new InstructionState(manager));
			}
			Debug.Log("In TutorialState");
        }
		/*
		void barraForza(float _now,float _min,float _max){
			GUIStyle boxVerde = new GUIStyle(GUI.skin.box);
			boxVerde.normal.background = manager.gameDataRef.boxVerde;
			boxVerde.hover.background = manager.gameDataRef.boxVerde;

			GUIStyle boxGiallo = new GUIStyle(GUI.skin.box);
			boxGiallo.normal.background = manager.gameDataRef.boxGiallo;
			boxGiallo.hover.background = manager.gameDataRef.boxGiallo;

			float inizH = Screen.width * 1 / 5; //punto d'inizio orizzontale
			float inizV = 10; //punto d'inizio verticale
			float largh = Screen.width*3/5;
			float sp = 30; //spessore
			_now = largh * _now;
			_min = largh * _min;
			_max = largh * _max;

			GUI.Box (new Rect(inizH,inizV,largh,sp),"",GUI.skin.box);
			GUI.Box (new Rect(inizH+_min-2,inizV-2,2,sp+4),"",GUI.skin.box);
			GUI.Box (new Rect(inizH+_max-2,inizV-2,2,sp+4),"",GUI.skin.box);

			if (_now >= _max) {
				GUI.Box (new Rect (inizH, inizV, _now, sp), "", boxGiallo);	//arancio
			} else {
				if (_now <= _min) {
					GUI.Box (new Rect (inizH, inizV, _now, sp), "", boxGiallo);	//arancio
				} else {
					GUI.Box (new Rect (inizH, inizV, _now, sp), "", boxVerde);	//verde
				}
			}
		}*/

		void CaricaScena(){
			canvas = GameObject.Instantiate(UnityEngine.Resources.Load("prefabs/Canvas")) as GameObject;  // find CastBar object
			forceBar = GameObject.Find ("CastBar") as GameObject;
			forceBarScript = forceBar.GetComponent<CastingBar>();   // assign CastingBar script
			clockTimer = GameObject.Instantiate(UnityEngine.Resources.Load("prefabs/alarmClockBlue")) as GameObject; // find ClockTimer object
			clockTimer.transform.position = new Vector3 (-3f, 7.6f, -4.59f);
			clockTimer.transform.eulerAngles = new Vector3(280f, -10f, 160f);
			clockTimer.transform.localScale = new Vector3(4f, 4f, 4f);
			clockScript = clockTimer.GetComponent<ClockScript>();   // assign clock timer script
			clockScript.InitArrows ();
			clockTimer.SetActive(false);
			canvas.SetActive(false);

		}

    }
}