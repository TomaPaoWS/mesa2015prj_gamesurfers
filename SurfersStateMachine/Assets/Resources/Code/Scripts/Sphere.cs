﻿using UnityEngine;
using System.Collections;

/// <summary>
/// bullet class
/// </summary>
public class Sphere : MonoBehaviour {
    
    /// <summary>
    /// final time
    /// </summary>
    private float futureTime = 0;
    /// <summary>
    /// delta time
    /// </summary>
    private int countDown = 0;
    /// <summary>
    /// time after that sphere destroied ( in seconds )
    /// </summary>
    private float sphereDuration = 8;

    /// <summary>
    /// Use this for initialization. set the initial time value.
    /// </summary>
	void Start () {
        // set up the final time
        futureTime = sphereDuration + Time.realtimeSinceStartup;
	}
	
    /// <summary>
    /// Update is called once per frame.
    /// </summary>
    /// <remarks>update timer and if countdown finished delte the object</remarks>
	void Update () {
        // actual time
        float rightNow = Time.realtimeSinceStartup;

        // new delta time
        countDown = (int)futureTime - (int)rightNow;

        // if count down less than 0 destroy sphere object
        if (countDown < 0) Destroy(gameObject);
	}
}
