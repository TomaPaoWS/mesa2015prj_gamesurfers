﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Assets.Resources.Code.Interfaces;


namespace Assets.Resources.Code.Scripts
{
    /// <summary>
    /// class of game data
    /// </summary>
    /// <remarks>memorize here all the data used in this game</remarks>
    public class GameData : MonoBehaviour, IGameData
    {
        /// <summary>
        /// Texture of the begin state backgorund
        /// </summary>
        public Texture2D beginStateSplash;
		public Texture2D title;
		public Texture2D btON;
		public Texture2D btOFF;
		public Texture2D boxForza;
		public Texture2D boxTra;
		public Texture2D boxVerde;
		public Texture2D boxGiallo;
		public Texture2D caffe0;
		public Texture2D caffe1;
		public Texture2D caffe2;
		public Texture2D caffe3;
		public Texture2D caffe4;
		public Texture2D caffe5;
		public Texture2D caffe6;
		public Texture2D caffe7;
		public Texture2D setting;
		public Texture info;
		public Texture2D nero;
		public Texture2D pulsanti;
		public int nPartita = 1;


		public float[] parFloat = {0.1f,0.9f,0.2f,0.5f,0.6f,0.8f,2f,2f};
		public bool[] parBool = {true,false,false,false,false,true,true};

		// timePC,timeHuman,spacePC,spaceHuman,rilasciTrascinamento,rilasciSalto,ostacoliCorretti
		public float[] risultFloat = {0,0,0,0,0,0,0};

		// primoOK,ultimoOK,finito
		public bool[] risultBool = {false,false,false};

		public Vector4 sfondoGUI = new Vector4 (15f/255f, 50f/255f, 15f/255f, 0f/255f);
        /// <summary>
        /// Texture of the begin state backgorund
        /// </summary>
        public Texture2D setupStateSplash;
        /// <summary>
        /// Texture of the setup state backgorund
        /// </summary>
        public Texture2D instructionStateSplash;
        /// <summary>
        /// Texture of the paly state backgorund
        /// </summary>
        public Texture2D playStateSplash;
        /// <summary>
        /// Texture of the won state backgorund
        /// </summary>
        public Texture2D wonStateSplash;
        /// <summary>
        /// Texture of the lost state backgorund
        /// </summary>
        public Texture2D lostStateSplash;

        
        /// <summary>
        /// class to manager Force input
        /// </summary>
        public ForceManager forceManager;

        /// <summary>
        /// color of the bar when under limit
        /// </summary>
        public Color lowForceColor = Color.yellow;
        /// <summary>
        /// color of the bar when out of limit
        /// </summary>
        public Color outForceColor = Color.red;
        /// <summary>
        /// color of bar when in the limit
        /// </summary>
        public Color okForceColor = Color.green;

        /// <summary>
        /// value of output pin of first Arduino button
        /// </summary>
        bool arduinoButtonA;
        /// <summary>
        /// get and set arduino first button output
        /// </summary>
        public bool ArduinoButtonA
        {
            get { return arduinoButtonA; }
            set { arduinoButtonA = value; }
        }

        /// <summary>
        /// value of output pin of second Arduino button
        /// </summary>
        bool arduinoButtonB;
        /// <summary>
        /// get and set arduino first button output
        /// </summary>
        public bool ArduinoButtonB
        {
            get { return arduinoButtonB; }
            set { arduinoButtonB = value; }
        }

        /// <summary>
        /// Use this for initialization
        /// </summary>
        void Start()
        {
            //TO DO: when scene start set default value of player lives
            forceManager = new ForceManager();
        }

        /// <summary>
        /// Update is called once per frame
        /// </summary>
        void Update()
        {
        }

        /// <summary>
        /// Reset game parameter to initial value
        /// </summary>
        public void ResetGameData()
        {
            //TO DO: when scene Restart set default value of player lives
            forceManager.ResetForce();
        }

    }
}