﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Assets.Resources.Code.Scripts;

/// <summary>
/// Custom bar
/// </summary>
/// <remarks>This class manage the customized bar object. you can set the position of the bar and also
/// two bar to indicate the upper and lower threshold</remarks>
public class CastingBar : MonoBehaviour {
    /// <summary>
    /// initial position of bar value
                      /// </summary>
	/// private Vector3 startPos;
    /// <summary>
    /// end position of bar value ( bar completly filled with color )
    /// </summary>
    private float endPos;
    /// <summary>
    /// initial position of threshold limit value ( lef side of the bar = 0 value ) 
    /// </summary>
    private Vector3 startPosThresholds;
    /// <summary>
    /// end position of threshold limit value ( right side of the bar = max value ) 
    /// </summary>
    private Vector3 endPosThresholds;

    /// <summary>
    /// initial position of bar value
    /// </summary>
    private RectTransform castTransform;

    /// <summary>
    /// image in the bar
    /// </summary>
    private Image CastImage;

    /// <summary>
    /// image of lower limit
    /// </summary>
    public Image lowThresholdImage;
    /// <summary>
    /// image of upper limit
    /// </summary>
    public Image upperThresholdImage;
    /// <summary>
    /// text box to show actual value in %
    /// </summary>
    public Text castName;
    /// <summary>
    /// text box on left side of bar now not used
    /// </summary>
    public Text castName1;

    /// <summary>
    /// color if I'm under range
    /// </summary>
    public Color lowForceColor = Color.yellow;
    /// <summary>
    /// color if I'm over range
    /// </summary>
    public Color outForceColor = Color.red;
    /// <summary>
    /// color if I'm in range
    /// </summary>
    public Color okForceColor = Color.green;

	/// <summary>
    ///	Use this for initialization 
	/// </summary>
		void Start () {
            // take rect attached to the bar
			castTransform = GetComponent<RectTransform> ();
		//Debug.Log (castTransform);
            // take the Image of the bar
            CastImage = GetComponent<Image> ();
            // set the end position ( it's ok because at the begining the bar is fullfilled by default )
			endPos = castTransform.localScale.x;
            //// set initial position of the bar ( bar empty )
            //startPos = 0f;
            //// set initial position of the two limit ( ok because at beginning the are on left side of the bar )
            //startPosThresholds = lowThresholdImage.rectTransform.position;
            //// set the end position of limit
            //endPosThresholds = new Vector3(lowThresholdImage.rectTransform.position.x + castTransform.rect.width, lowThresholdImage.rectTransform.position.y, lowThresholdImage.rectTransform.position.z);
        }

    /// <summary>
    /// Setting the value of bar lower and upper limit range and the actual bar value ( ususally 0 )
    /// </summary>
    /// <param name="lowerThreshold_tmp"></param>
    /// <param name="upperThreshold_tmp"></param>
    /// <param name="actualValue_tmp"></param>
        public void InitializeForceBar(float lowerThreshold_tmp, float upperThreshold_tmp, float actualValue_tmp) {

		// positoning the bar to the actual force value
		Vector3 app;
		app = new Vector3 (endPos * actualValue_tmp * 0.1f / 100f, 1, 1);
		castTransform.localScale = app;
            // show the force value in %
		castName.text = (actualValue_tmp * 0.1f).ToString("F1") + "%";
            // set the lower limit range value

		//endPos * lowerThreshold_tmp * 0.1f / 100f
		app = new Vector3 (50f, 0f, 0f);
		lowThresholdImage.rectTransform.anchoredPosition = new Vector2(endPos * lowerThreshold_tmp * 0.1f / 100f,0.5f);
            // set the upper limit range value
		app = new Vector3 (400f, 0f, 0f);
		upperThresholdImage.rectTransform.anchoredPosition = new Vector2(endPos * upperThreshold_tmp * 0.1f / 100f,0.5f);
            // set initial color to the lower value color
        CastImage.color = lowForceColor;
        }

    /// <summary>
    /// Function recalled to update bar value
    /// </summary>
    /// <param name="upperThreshold_tmp"> upper value of bar validation range ( 0 to 1000 )</param>
        public void UpdateCastBar(float actualValue_tmp) {
			//Debug.Log ("CastingBar.UpdateCastBar");    

		// positoning the bar to the actual force value
			Vector3 app;
			app = new Vector3 (endPos * actualValue_tmp * 0.1f / 100f, 1, 1);
			castTransform.localScale = app;
            // show the force value in %
            castName.text = (actualValue_tmp * 0.1f).ToString("F1") + "%";
    	}

    /// <summary>
    /// update color of the bar
    /// </summary>
    /// <param name="value_tmp"></param>
        public void UpdateCastBarColor(int value_tmp) {
		//Debug.Log ("CastingBar.UpdateCastBarColor");
			if (value_tmp == 0) CastImage.color = lowForceColor;
            else if (value_tmp == 1) CastImage.color = okForceColor;
            else CastImage.color = outForceColor;
        }
	}
