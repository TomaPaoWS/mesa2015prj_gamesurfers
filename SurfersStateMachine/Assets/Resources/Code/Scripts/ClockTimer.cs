﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Class associated to the clock timer object
/// </summary>
public class ClockTimer : MonoBehaviour {

    /// <summary>
    /// clock object, take clock prefab
    /// </summary>
    public GameObject clock;
    /// <summary>
    /// initialize the clock timer object
    /// </summary>
	void Start () {
        // create new timer object form prefabs
		GameObject clockInstance = (GameObject) Instantiate(clock, new Vector3 ( 4.5f, 1.285f, -9.06f ), Quaternion.identity); // instantiate new clock prefab object
        clockInstance.transform.Rotate(new Vector3(290, 180, 0));
	}
}
