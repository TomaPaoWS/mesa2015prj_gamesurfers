﻿using UnityEngine;
using System.Collections;

public class turbinio : MonoBehaviour {


	int segmentit = 1000;
	GameObject lineagt;
	LineRenderer lineat;
	float r_circ = 1f;
	float corona = 0.2f;
	float ratio;
	float spessore = 0.1f;
	float altezza_t;
	float giri;
	float T = 0.5f;
	int flag;


	float xt,zt,yt,anglet;
	// Use this for initialization
	void Start () {
		altezza_t = 2 * r_circ;
		giri = altezza_t / (1.2f * spessore);
		ratio = 1.5f * spessore / 360f;
		lineagt = Instantiate (Resources.Load ("linea_circ")) as GameObject;
		lineagt.transform.position = new Vector3(0f,-altezza_t,0f);
		lineat = lineagt.GetComponent<LineRenderer> ();
		lineat.SetWidth (0.1f, 0.1f);
		lineat.useWorldSpace = false;
		lineat.SetVertexCount(segmentit+1);


		anglet = 0;

		for (int i = 0; i < segmentit + 1; i++) {

			xt =   Mathf.Sin (Mathf.Deg2Rad * anglet) * (r_circ-corona);
			zt =   Mathf.Cos (Mathf.Deg2Rad * anglet) * (r_circ-corona);
			yt =  anglet*ratio;
								
			lineat.SetPosition (i,new Vector3(xt,yt,zt) );
						
			anglet += ((giri*360f) / segmentit);
			
		}
		flag = 0;


	
	}
	
	// Update is called once per frame
	void Update () {
		if (flag == 0) {
			if (lineagt.transform.position.y < 0f) {
				lineagt.transform.Translate(0f,altezza_t*(Time.deltaTime/T),0f);
				lineagt.transform.Rotate(0f,(giri*360f)*(Time.deltaTime/T),0f);
			}
			else {
				flag = 1;

			}
		}
		if (flag == 1 && (lineagt.transform.position.y > (-altezza_t - 0.5f))) {
			lineagt.transform.Translate (0f, -altezza_t * (Time.deltaTime / T), 0f);
			lineagt.transform.Rotate (0f, -(giri * 360f) * (Time.deltaTime / T), 0f);

		}



	
	}
}
