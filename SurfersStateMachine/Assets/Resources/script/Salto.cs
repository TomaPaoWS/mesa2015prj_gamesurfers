﻿using UnityEngine;
using System.Collections;

public class Salto : StateMachineBehaviour {


	GameObject myPersonaggio;
	Vector3 initialPosition;
	bool movimentoFinito;

	 // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		myPersonaggio = animator.gameObject;
		initialPosition = myPersonaggio.transform.position;
		animator.SetBool ("ST_saltaEND",false);
		movimentoFinito = false;
	}

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
	override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {

		// "Vsalto" è un parametro legato ad una curva dell'animazione jump. la curva segue l'altezza del centro di massa..per spostamento verticale
		// "Hsalto" è un parametro legato ad una curva dell'animazione jump. cresce linearmente da 0 ad 1 mentre l'omino è in volo...per spostamento orizzontale
		// "Vmax" è l'altezza del salto desiderata
		// "Hmax" è la lunghezza del salto desiderata
		
		// SISTEMAZIONE ALTEZZA E LUNGHEZZA SALTO

		if (animator.GetFloat ("blendJump")==-1f || animator.GetFloat ("blendJump")==2f) {
			//animator.SetFloat ("velSalto", 1f);
			animator.SetBool ("ST_salta",false);
			return;
		}
		Vector3 actualPosition = initialPosition;
		float actualDirection = myPersonaggio.transform.eulerAngles.y;
		float altezzaAggiunta = Mathf.Max (0f, animator.GetFloat ("Vmax") - 0.2f); // di suo salta in alto 0.42;
		actualPosition.x += Mathf.Sin(actualDirection*Mathf.PI/180f) * animator.GetFloat ("Hsalto") * animator.GetFloat ("Hmax");
		actualPosition.z += Mathf.Cos(actualDirection*Mathf.PI/180f) * animator.GetFloat ("Hsalto") * animator.GetFloat ("Hmax");
		actualPosition.y += animator.GetFloat ("Vsalto") * altezzaAggiunta;
		//dopo che atterra non aggiorno più; altrimenti nella transizione di uscita tornava indietro.
		if (!movimentoFinito) { myPersonaggio.transform.position = actualPosition;} 
		if (animator.GetFloat ("Hsalto") == 1) {
			movimentoFinito=true;
			animator.SetBool ("ST_salta",false);
		}

		// "velSalto" è legato alla velocità dell'animazione jump.
		// SISTEMAZIONE DURATA SALTO
		float velox = Mathf.Max(0.2f, 1f - (0.2f * animator.GetFloat ("Vsalto") * altezzaAggiunta)); 
		//animator.SetFloat ("velSalto", velox);
	}

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
	override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		animator.SetBool ("ST_saltaEND",true);
	}

	// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
	//override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
	//override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}
}
