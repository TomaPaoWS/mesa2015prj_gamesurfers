﻿using UnityEngine;
using System.Collections;

public class Ostacolo : Object {

	public GameObject myOstacolo;
	public float r_circ;
	public float altezza;
	public float forza;
	public float forzaLim;
	public bool vuoto = false;
	public int salto;
	static int linee = 7;
	static int campioni = 1000;

	GameObject[] lineag = new GameObject[linee];
	LineRenderer[] linea = new LineRenderer[linee];
	int segmenti = 1000;
	Material[] matl = new Material[linee];
	static int linee_inter = 5;
	//float par_int = 0.9f;
	float angle,j,x,z,x1,z1,t; 
	float[,,] xp = new float[campioni,linee_inter,2];
	float[,,] zp = new float[campioni,linee_inter,2];
	MeshRenderer[] mrt;
	Material[] mat; 
	Material agg;
	Color aggc;

	Color[] lineac = new Color[linee];
	Color[] colori;

	Color circ = new Color (1f, 0.973f, 0f, 0.392f);

	int segmentit = 1000;
	GameObject lineagt;
	LineRenderer lineat;
	float ratio;
	float spessore = 0.1f;
	float altezza_t;
	float giri;
	float T = 0.5f;
	float rtc = 5f;
	int flag;

	int cont;
	int flagt;
	float corona = 0.15f;

	float xt,zt,yt,anglet;

	// Inizializzatore
	public Ostacolo(Vector3 _posizione,Vector3 _orientazione,string _prefab,float _r_circ,float _forza, float _forzaLim,float _altezza,int _tiposalto) {

		salto = _tiposalto;
		r_circ = _r_circ;
		altezza = _altezza;
		forza = _forza;
		forzaLim = _forzaLim;
		myOstacolo = Instantiate (Resources.Load (_prefab)) as GameObject;
		myOstacolo.transform.position = _posizione;
		myOstacolo.transform.eulerAngles = _orientazione;
		myOstacolo.tag ="ostacolo";

		//matl = Resources.Load ("circonferenza", typeof(Material)) as Material;

		for (int i=0; i<linee; i++) {
			lineag [i] = Instantiate (Resources.Load ("linea_circ")) as GameObject;
			lineag[i].tag = "ostacolo";
			linea [i] = lineag [i].GetComponent<LineRenderer> ();
			linea [i].SetWidth (0.05f, 0.05f);
			matl[i] = linea[i].material;
			lineac[i] = matl[i].GetColor("_TintColor");


		}

		linea[0].SetVertexCount(segmenti+1);
		linea[1].SetVertexCount(segmenti+1);



		j = 0;
		cont = 0;
		angle = 0;
		for (int i = 0; i < segmenti + 1; i++) {
		
			x = myOstacolo.transform.position.x + Mathf.Sin (Mathf.Deg2Rad * angle) * _r_circ;
			z = myOstacolo.transform.position.z + Mathf.Cos (Mathf.Deg2Rad * angle) * _r_circ;
			x1 = myOstacolo.transform.position.x + Mathf.Sin (Mathf.Deg2Rad * angle) * (_r_circ-corona);
			z1 = myOstacolo.transform.position.z + Mathf.Cos (Mathf.Deg2Rad * angle) * (_r_circ-corona);
			if (Mathf.Abs(j- angle) < (360f / segmenti) && cont < linee_inter) {
				//Debug.Log(j);
				linea[2+cont].SetPosition (0,new Vector3(x1,0.1f,z1) );
				linea[2+cont].SetPosition (1,new Vector3(x,0.1f,z) );

				j += (360f / linee_inter);
				cont += 1;
			}
			
			linea[0].SetPosition (i,new Vector3(x,0.1f,z) );
			linea[1].SetPosition (i,new Vector3(x1,0.1f,z1) );
			
			angle += (360f / segmenti);
		

			
		}


		//turbinio

		altezza_t =  1.6f * r_circ;
		giri = altezza_t / (1.2f * spessore);
		ratio = 1.2f * spessore / 360f;
		lineagt = Instantiate (Resources.Load ("lineat")) as GameObject;
		lineagt.transform.position = new Vector3(myOstacolo.transform.position.x,myOstacolo.transform.position.y-1.1f*altezza_t,myOstacolo.transform.position.z);

		lineat = lineagt.GetComponent<LineRenderer> ();
		lineat.SetWidth (0.1f, 0.1f);
		lineat.useWorldSpace = false;
		lineat.SetVertexCount(segmentit+1);
		
		
		anglet = 0;
		
		for (int i = 0; i < segmentit + 1; i++) {
			
			xt =   Mathf.Sin (Mathf.Deg2Rad * anglet) * (r_circ-corona);
			zt =   Mathf.Cos (Mathf.Deg2Rad * anglet) * (r_circ-corona);
			yt =  anglet*ratio;
			
			lineat.SetPosition (i,new Vector3(xt,yt,zt) );
			
			anglet += ((giri*360f) / segmentit);
			
		}
		flagt = 0;



		//array seno
		angle = 0; 
		t = 0;
		for (int p = 0;p<campioni;p++) {
			for (int k = 0;k < linee_inter;k++) {
				xp[p,k,0] = myOstacolo.transform.position.x + Mathf.Sin (Mathf.Deg2Rad * (angle+360f/linee_inter*k)) * (_r_circ-corona);
				xp[p,k,1] = myOstacolo.transform.position.x + Mathf.Sin (Mathf.Deg2Rad * (angle+360f/linee_inter*k)) * _r_circ;
				zp[p,k,0] = myOstacolo.transform.position.z + Mathf.Cos (Mathf.Deg2Rad * (angle+360f/linee_inter*k)) * (_r_circ-corona);
				zp[p,k,1] = myOstacolo.transform.position.z + Mathf.Cos (Mathf.Deg2Rad * (angle+360f/linee_inter*k)) * _r_circ;
			}
			t += 2*Mathf.PI/campioni;
			angle = 90f*Mathf.Sin(t);
		}

		mrt = myOstacolo.GetComponentsInChildren<MeshRenderer> ();
		mat = new Material[mrt.Length];
		colori = new Color[mrt.Length];
		for (int i = 0; i<mrt.Length; i++) {
			mat [i] = mrt [i].material;
			colori[i] = mat [i].color;
			if (mrt[i].materials.Length > 1) {
				flag = 1;
				agg = mrt[i].materials[1];
				//Debug.Log(agg.name);
				aggc = agg.color;
			}
			
			
			
		}


	}

	public void psyco(float _time,float _T) {
		int passo = (int)(((_time % _T) / _T) * campioni);
			
		for (int k = 0; k < linee_inter; k++) {
			linea [2 + k].SetPosition (0, new Vector3 (xp [passo, k, 0], 0.1f, zp [passo, k, 0]));
			linea [2 + k].SetPosition (1, new Vector3 (xp [passo, k, 1], 0.1f, zp [passo, k, 1]));
		}
			
	}

	public void changecolor(bool _sigira) {
		if (_sigira) {
			for (int i = 0; i < linee; i++) {
				linea [i].material.SetColor ("_TintColor", circ);
			}
		} 
		else {
			for (int i = 0; i < linee; i++) {
				linea [i].material.SetColor ("_TintColor", lineac[0]);
			}
		}
		
	}

	public void distruggi(float _T) { 


		//turbinio
		if (flagt == 0) {
			if (lineagt.transform.position.y < 0f) {
				lineagt.transform.Translate(0f,altezza_t*(Time.deltaTime/(_T/rtc)),0f);
				lineagt.transform.Rotate(0f,(giri*360f)*(Time.deltaTime/(_T/rtc)),0f);
			}
			else {
				flagt = 1;
				
			}
		}
		if (flagt == 1 && (lineagt.transform.position.y > (-altezza_t - 0.5f))) {
			lineagt.transform.Translate (0f, -altezza_t * (Time.deltaTime / (_T/rtc)), 0f);
			lineagt.transform.Rotate (0f, -(giri * 360f) * (Time.deltaTime / (_T/rtc)), 0f);
			
		}
		//---------------
		if (flagt == 1) {
			for (int i = 0; i<mrt.Length; i++) {
				if (mat [i].color.a == colori [i].a) {
					mat [i].shader = Shader.Find ("Transparent/Diffuse");
					
				}
				mat [i].color = new Color (colori [i].r, colori [i].g, colori [i].b, mat [i].color.a - Time.deltaTime / _T);

			}
			for (int i = 0; i<linee; i++) {
				matl [i].SetColor ("_TintColor", new Color (lineac [i].r, lineac [i].g, lineac [i].b, matl [i].GetColor ("_TintColor").a - lineac [i].a * Time.deltaTime / _T));
			}
			if (flag == 1) {
				if (agg.color.a == aggc.a) {
					agg.shader = Shader.Find ("Transparent/Diffuse");
					Debug.Log (agg.name);
					Debug.Log (agg.shader.name);
				}

			
				agg.color = new Color (aggc.r, aggc.g, aggc.b, agg.color.a - Time.deltaTime / _T);
			}


			if (mrt [0].material.color.a < 0.01f) {
				Destroy (myOstacolo);

				for (int i = 0; i < linee; i++) {
					Destroy (lineag [i]);
				}
				vuoto = true;
			}
		}
			//return false;
//		}

	}
}
	
	
