﻿using UnityEngine;
using System.Collections;

public class Personaggio : Object{
	
	//questo è il prefab da caricare
	public GameObject myPersonaggio;
	private Animator animator;
	
	// Use this for initialization
	public Personaggio(Vector3 _posIniziale,Vector3 _posOstacolo1) {
		Debug.Log("sono in: inizializzazione Personaggio");
		GameObject prefab = Resources.Load<GameObject>("characters/PlayerLUI");
		myPersonaggio = (GameObject) Instantiate(prefab);
		myPersonaggio.transform.position = _posIniziale;
		myPersonaggio.transform.LookAt(_posOstacolo1);
		//myPersonaggio.transform.localScale = new Vector3 (0.6f, 0.6f, 0.6f);
		animator = myPersonaggio.GetComponent<Animator> ();
	}

	
	// parametri all'interno di "animControl"
	public bool ANIM_risc {
		get { return animator.GetBool("ST_risc");}
		set { animator.SetBool("ST_risc",value); }
	}
	public bool ANIM_fest {
		get { return animator.GetBool("ST_festeggia");}
		set { animator.SetBool("ST_festeggia",value); }
	}
	public bool ANIM_carSalto {
		get { return animator.GetBool("ST_caricaSalto");}
		set { animator.SetBool("ST_caricaSalto",value); }
	}
	private float SPEED {
		get { return animator.GetFloat("planeSpeed");}
		set { animator.SetFloat("planeSpeed",value); }
	}
	private bool ANIM_salta {
		get { return animator.GetBool("ST_salta");}
		set { animator.SetBool("ST_salta",value); }
	}
	private bool ANIM_saltaEND {
		get { return animator.GetBool("ST_saltaEND");}
		set { animator.SetBool("ST_saltaEND",value); }
	}
	private float lunghezzaSalto {
		get { return animator.GetFloat("Hmax");}
		set { animator.SetFloat("Hmax",value); }
	}
	private float altezzaSalto {
		get { return animator.GetFloat("Vmax");}
		set { animator.SetFloat("Vmax",value); }
	}
	private float animSalto {
		get { return animator.GetFloat("blendJump");}
		set { animator.SetFloat("blendJump",value); }
	}

	private float velSalto {
		get { return animator.GetFloat("velSalto");}
		set { animator.SetFloat("velSalto",value); }
	}
	
	// Gira attorno all'ostacolo e si orienta***************************************************************
	Vector3 ostacoloVicino;
	Vector3 prossimoOstacolo;
	bool guardaEXT;
	public bool Girotondo(Vector3 _ostacoloVicino,Vector3 _prossimoOstacolo,bool _guardaEXT) {
		//Debug.Log ("sono in: Personaggio.Girotondo");
		ostacoloVicino = _ostacoloVicino;
		prossimoOstacolo = _prossimoOstacolo;
		guardaEXT = _guardaEXT;
		SPEED = 2f;

		Vector3 posizioneAttualeREL;
		Vector3 prossimoOstacoloREL;
		Vector3 direction;
		float dirPerpendicolare;
		float angoloProssimoOstacolo;
		float angoloAttuale;
		bool termina;

		// decidi da che parte andare
		posizioneAttualeREL = myPersonaggio.transform.position - ostacoloVicino;
		prossimoOstacoloREL = prossimoOstacolo - ostacoloVicino;
		direction = Vector3.Cross (posizioneAttualeREL, prossimoOstacoloREL);
		dirPerpendicolare = 90f;
		if (direction.y < 0) {
			dirPerpendicolare = -90f;
		}

		// calcola angolo finale
		angoloProssimoOstacolo = Mathf.Atan2 (prossimoOstacoloREL.x, prossimoOstacoloREL.z) * 180f / Mathf.PI;
	
		// mentre sta girando
		posizioneAttualeREL = myPersonaggio.transform.position - ostacoloVicino;
		angoloAttuale = Mathf.Atan2 (posizioneAttualeREL.x, posizioneAttualeREL.z) * 180f / Mathf.PI;
		myPersonaggio.transform.eulerAngles = new Vector3 (0, angoloAttuale + dirPerpendicolare, 0);


		// terminazione
		termina = false;
		if (Mathf.Abs(angoloAttuale-angoloProssimoOstacolo)<5f){	
			Debug.Log ("Girotondo finito");
			SPEED = 0f;
			termina = true;
			if (_guardaEXT) { myPersonaggio.transform.LookAt(prossimoOstacolo);}
			           else { myPersonaggio.transform.LookAt(ostacoloVicino);}
		}
		return termina;
	}
	public bool Girotondo() {
		return Girotondo(ostacoloVicino,prossimoOstacolo,guardaEXT);
	}

	public void Carica(float _forza){
		animSalto = 1f;
		velSalto = 0f;
		animator.Play ("Salto",-1, _forza*0.25f);
	}


	// Raggiungi prossimo ostacolo**************************************************************************
	Vector3 nextOstacolo = new Vector3 ();
	float distMin = 0f;
	public bool Raggiungi(Vector3 _nextOstacolo,float _speed,float _distMin) {

		//Debug.Log ("sono in: Personaggio.Raggiungi");
		nextOstacolo = _nextOstacolo;
		SPEED = _speed;
		distMin = _distMin;

		//si orienta
		myPersonaggio.transform.LookAt (_nextOstacolo);

		// terminazione
		if (Vector3.Distance (_nextOstacolo, myPersonaggio.transform.position) < _distMin) {
			Debug.Log ("Raggiungi finito");
			SPEED = 0;
		}
		return (SPEED == 0);
	}
	public bool Raggiungi() {
		return Raggiungi (nextOstacolo, SPEED, distMin);
	}
	
	//Salta l'ostacolo**************************************************************************************
	float anim = 0f;
	public bool Salta(float _altezza,float _lunghezza, float _anim) {
		//Debug.Log ("sono in: Personaggio.Salta");
		altezzaSalto = _altezza;
		lunghezzaSalto = _lunghezza;
		animSalto = _anim;
		ANIM_salta = true;
		ANIM_saltaEND = false;
		velSalto = 1f;
		return ANIM_saltaEND;
	}
	public bool Salta (){
		return ANIM_saltaEND;
	}

	//Cammina***********************************************************************************************
	public void Cammina(float _speed) {
		Debug.Log ("Cammina");
		SPEED = _speed;
	}

	//Stop**************************************************************************************************
	public void Stop() {
		Debug.Log ("Stop");
		Raggiungi(myPersonaggio.transform.position,1f,100f);
	}

	//Guarda************************************************************************************************
	public void Guarda(Vector3 _nextOstacolo) {
		Debug.Log ("Guarda");
		_nextOstacolo.y = myPersonaggio.transform.position.y;
		myPersonaggio.transform.LookAt (_nextOstacolo);
	}
}

