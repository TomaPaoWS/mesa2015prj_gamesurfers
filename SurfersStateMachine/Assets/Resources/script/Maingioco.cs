﻿using UnityEngine;
using System.Collections;
 

public class Maingioco : MonoBehaviour {

	private StateManager manager = new StateManager();


	// parametri
	public static int n_o = 5;
	int tipi_oggetto = 6;
	float soglia = 0.5f; // soglia radianti tra rette
	float rapp = (float)(n_o + 1); //indica la frazione della diagonale d'ambiente che da la vicinanza max tra oggetti
	float v_linea = 3f;
	float cornice = 0.65f;
	

	float attesa_start = 5f;
	float attesa_jump = 1.5f;
	float attesa_fase2 = 5f;
	float soglia_pick = 2f;
	float fade_ostacoli =  3f;

	float force_limit1 = 1f;
	float force_limit2 = 2f;
	float force_limit3 = 3f;
	float force_limit4 = 4f;

	Vector3 sut = new Vector3 (0f, 0.2f, 0f);
	Vector3 startc_p = new Vector3 (0f, 8.42f, -8.53f);
	Vector3 stopc_p = new Vector3 (6.55f, 1.38f, 2.51f);
	Vector3 startc_r = new Vector3 (52f, 0f, 0f);
	Vector3 stopc_r = Vector3.zero;
	int conttel = 0;
	float Tcam = 4f;


	GameObject ambiente;
	Ostacolo[] oggetti = new Ostacolo[n_o]; 
	Vector3 tVector1,tVector2,tVector3,p;



	LineRenderer[] linee = new LineRenderer[n_o + 1];
	GameObject[] lineeg = new GameObject[n_o + 1];
	float[] track = new float[n_o + 1];
	Vector3[] posizione = new Vector3[n_o + 2];


	float rx,rz,ang1,ang2,ang3,dist,counter,t,ssx,ssy,ssz,max_forza;
	public float forza = 0;

	float[] r_circT = {1.7f,1.55f,1.5f,0.8f,0.6f,0.6f};
	float[,] forzeT = new float[8,2];
	float[] altezzeT = {2f,2f,2f,1f,1f,1f};
	int[] tipo_saltoT = {1,1,1,0,0,0};


	int cont,flag,actualForce,tOggetto,stato;
	bool firstTime=true;

	bool c1m,c2m;


	Personaggio boy;

	bool girotondoInEsecuzione,raggiungiInEsecuzione,SaltoInEsecuzione;

	Camera myCamera;
	float[] parFloat;
	bool[] parBool;


		
	// metodo di inizializzaione
	void Start () {
		myCamera = Camera.main;

		manager = FindObjectOfType<StateManager>();
		n_o = Mathf.RoundToInt(manager.gameDataRef.parFloat [6]);
		forzeT[0,0] = manager.gameDataRef.parFloat [4];
		forzeT[1,0] = manager.gameDataRef.parFloat [4];
		forzeT[2,0] = manager.gameDataRef.parFloat [4];
		forzeT[3,0] = manager.gameDataRef.parFloat [2];
		forzeT[4,0] = manager.gameDataRef.parFloat [2];
		forzeT[5,0] = manager.gameDataRef.parFloat [2];

		forzeT[0,1] = manager.gameDataRef.parFloat [5];
		forzeT[1,1] = manager.gameDataRef.parFloat [5];
		forzeT[2,1] = manager.gameDataRef.parFloat [5];
		forzeT[3,1] = manager.gameDataRef.parFloat [3];
		forzeT[4,1] = manager.gameDataRef.parFloat [3];
		forzeT[5,1] = manager.gameDataRef.parFloat [3];


		myCamera.transform.position = new Vector3 (0f, 8.42f, -8.53f); //per visione 3d
		//transform.position = new Vector3 (-0.1f, 9.6f, -0.1f); //per visione alta
		myCamera.transform.eulerAngles = new Vector3 (52f, 0f, 0f);// per visione 3d
		//transform.eulerAngles = new Vector3 (90f, 0f, 0f);/per visione alta

		// inizializzo ambiente
		ambiente = Instantiate (Resources.Load ("ambiente")) as GameObject;
		ambiente.transform.position = new Vector3 (0f, 0f, 0f);
		ambiente.transform.eulerAngles = new Vector3 (0f, 0f, 0f);

		posizione[0] = ambiente.transform.GetChild (0).position;
		posizione[n_o+1] = ambiente.transform.GetChild (1).position;

		//dist = Mathf.Sqrt(((posizione[0].x - posizione[n_o+1].x)*(posizione[0].z - posizione[n_o+1].z)/(float)rapp));
		dist = Mathf.Max (Vector3.Distance(posizione[0],posizione[n_o+1])/rapp,2f*Mathf.Max(r_circT));

		parFloat = manager.gameDataRef.parFloat;
		parBool = manager.gameDataRef.parBool;


		//ASSEGNAZIONE OGGETTI
		//assegnazione primo oggetto
		flag = 0;
		while (flag == 0) {
			rx  = Random.Range (posizione[0].x+cornice, posizione[n_o+1].x-cornice);
			rz  = Random.Range (posizione[0].z+cornice, posizione[n_o+1].z-cornice);
			if ((Vector3.Distance(new Vector3(rx,0f,rz),posizione[0]) > 2f*dist) && (Vector3.Distance(new Vector3(rx,0f,rz),posizione[n_o+1]) > dist)) {
				flag = 1;
			} 
		}
			
		posizione [1] = new Vector3 (rx , 0f, rz );
		tVector1 = posizione[1] - posizione[0];

		//scelta oggetto
		tOggetto = Random.Range (0, tipi_oggetto-1);

		//creazione oggetto
		oggetti [0] = new Ostacolo (posizione [1], new Vector3 (0f, Mathf.Rad2Deg * (-Mathf.Atan2 (tVector1.z, tVector1.x)), 0f), "Oggetto" + (tOggetto + 1), r_circT [tOggetto], forzeT [tOggetto,0], forzeT [tOggetto,1],altezzeT [tOggetto],tipo_saltoT[tOggetto]);

		//posiziono personaggio
		boy = new Personaggio (posizione[0],posizione[1]);

				//assegnazione altri oggetti
		for (int i=2; i < n_o+1; i++) {
			flag = 0;
			while (flag == 0) {
				flag = 1;
				rx  = Random.Range (posizione[0].x+cornice, posizione[n_o+1].x-cornice);
				rz  = Random.Range (posizione[0].z+cornice, posizione[n_o+1].z-cornice);
				posizione [i] = new Vector3 (rx , 0f, rz );
				//controllo distanze

				for (int j = 0; j < i-1;j++) {
					//distanza da oggetti
					if (Vector3.Distance(new Vector3(rx,0f,rz),posizione[j])  < dist) {
						flag  = 0;
					}
					//distanza da oggetto precedente
					if (Vector3.Distance(new Vector3(rx,0f,rz),posizione[i-1])  < 2f*dist) {
						flag  = 0;
					}
					//distanza da traguardo finale
					if (Vector3.Distance(new Vector3(rx,0f,rz),posizione[n_o+1]) < dist) {
						flag = 0;
					}
				}

				//controllo posizioni proibite
				if (flag == 1) {

					//controllo conflitti con oggetti già esistenti
					for (int j=0; j<i-1; j++) {
						//vettore oggetto -> altro_oggetto
						tVector1 = posizione[j] - posizione[i];
						ang1 = Mathf.Atan2 (tVector1.z,tVector1.x);
						ang1 = (ang1 +Mathf.PI*2f) % (Mathf.PI*2f);
						//vettore oggetto -> altro_oggetto+1
						tVector2 = posizione[j+1] - posizione[i];
						ang2 = Mathf.Atan2 (tVector2.z,tVector2.x);
						ang2 = (ang2 +Mathf.PI*2f) % (Mathf.PI*2f);
						//vettore oggetto-1 -> altro_oggetto
						tVector3 = posizione[j] - posizione[i-1];
						ang3 = Mathf.Atan2 (tVector3.z,tVector3.x);
						ang3 = (ang3 +Mathf.PI*2f) % (Mathf.PI*2f);
//						mtemp1 = (rz [i] - rz [j]) / (rx [i] - rx [j]);
//						mtemp2 = (rz [i] - rz [j + 1]) / (rx [i] - rx [j + 1]);
//						mtemp3 = (rz [i - 1] - rz [j]) / (rx [i - 1] - rx [j]);

						// condizioni di posizioni reciproche

						// oggetto in mezzo a altro oggetto e altro oggetto + 1
						//c1 = Mathf.Abs (ang1-ang2) > Mathf.PI/2f;

//						c1 = rz[i] < rz[j] && rz[ i] > rz[j+1] && rx[i] < rx[j] && rx[ i] > rx[j+1] ;
//						c2 = rz[i] > rz[j] && rz[ i] < rz[j+1] && rx[i] > rx[j] && rx[ i] < rx[j+1] ;
//						c1a = rz[i] < rz[j] && rz[ i] > rz[j+1] && rx[i] > rx[j] && rx[ i] < rx[j+1] ;
//						c2a = rz[i] > rz[j] && rz[ i] < rz[j+1] && rx[i] < rx[j] && rx[ i] > rx[j+1] ;

						//altro oggetto in mezzo a oggetto e oggetto precedente
						//c2 = Mathf.Abs ((ang1+Mathf.PI)%Mathf.PI*2f-(ang3+Mathf.PI)%Mathf.PI*2f) > Mathf.PI/2f;


//						c3 = rz[i-1] < rz[j] && rz[j] < rz[i] && rx[i-1] < rx[j] && rx[j] < rx[i] ;
//						c4 = rz[i-1] > rz[j] && rz[j] > rz[i] && rx[i-1] > rx[j] && rx[j] > rx[i] ;
//						c3a = rz[i-1] < rz[j] && rz[j] < rz[i] && rx[i-1] > rx[j] && rx[j] > rx[i] ;
//						c4a = rz[i-1] > rz[j] && rz[j] > rz[i] && rx[i-1] < rx[j] && rx[j] < rx[i] ;

						//controllo di soglia sulla condizione1
						c1m = Mathf.Abs(Mathf.Abs(ang1 - ang2) - Mathf.PI) < soglia;

//						c1m = (Mathf.Abs (Mathf.Abs (Mathf.Atan(mtemp1)) - Mathf.Abs (Mathf.Atan(mtemp2))) > soglia);

						//controllo di soglia sulla condizione1
						c2m = Mathf.Abs(Mathf.Abs((ang1+Mathf.PI)%(Mathf.PI*2f) - (ang3+Mathf.PI)%(Mathf.PI*2f)) - Mathf.PI) < soglia;


//						c2m = (Mathf.Abs (Mathf.Abs (Mathf.Atan(mtemp3)) - Mathf.Abs (Mathf.Atan(mtemp1))) > soglia);
						Debug.Log ("i"+i+"j"+j+"ang1_"+ang1+"ang2_"+ang2);

						// accetto la posizione per l'altro oggetto j-esimo
						if (c1m || c2m ) {
							flag = 0;
						}

						//se è l'ultimo oggetto va controllata anche posizione rispetto a traguardo
						if (i == n_o && flag == 1) {
							//vettore altro_oggetto+1 -> traguardo finale
							tVector1 = posizione[n_o+1] - posizione[j+1];
							ang1 = Mathf.Atan2 (tVector1.z,tVector1.x);
							ang1 = (ang1 +Mathf.PI*2f) % (Mathf.PI*2f);
							c1m = Mathf.Abs(Mathf.Abs(ang1 - (ang2+Mathf.PI)%(Mathf.PI*2f)) - Mathf.PI) < soglia;
							Debug.Log ("ang1"+ang1+"j"+j);
							if (c1m) {
								flag = 0;
							}
						}
						
						
					}


				}
			}		

			//scelgo tipo d'oggetto e creo il nuovo ostacolo
			tVector1 = posizione[i] - posizione[i-1];
			
			//scelta oggetto
			tOggetto = Random.Range (0, tipi_oggetto-1);
			
			//creazione oggetto
			oggetti [i-1] = new Ostacolo (posizione [i], new Vector3 (0f, Mathf.Rad2Deg * (-Mathf.Atan2 (tVector1.z, tVector1.x)), 0f), "Oggetto" + (tOggetto + 1), r_circT [tOggetto], forzeT [tOggetto,0], forzeT [tOggetto,1],altezzeT [tOggetto],tipo_saltoT[tOggetto]);
			         
//			oggetti[i - 1]  transform.position = new Vector3 (rx [i], 0f, rz [i]);
//			tVector = oggetti [i-1].transform.position - oggetti [i-2].transform.position;
//			oggetti[i-1].transform.Rotate(new Vector3(0f,-Mathf.Atan2 (tVector.z, tVector.x)*180f/Mathf.PI,0f));
		
		} // FINE FOR DI ASSEGNAZIONE

		//INIZIALIZZO UNA RETTA PER OGNI TRATTO

		//creo un vettore delle posizioni


//		posizione [0] = start;
//		for (i=1; i<n_o+1; i++) {
//			posizione [i] = oggetti [i-1].transform.position;
//			posizione[i].y = 0.1f;
//		}
//		posizione [n_o+1] = stop;

		//inizializzo linee

		for (int i=0; i<n_o+1; i++) {
			lineeg [i] = Instantiate (Resources.Load ("linea")) as GameObject;
			linee [i] = lineeg[i].GetComponent<LineRenderer>();
			linee[i].SetWidth(0.1f,0.1f);
		}
		


		track[0] = Vector3.Distance(posizione[0],posizione[1]);
		linee[0].SetPosition(0,posizione[0]+sut);
		linee[0].SetPosition(1,posizione[0]);
		for (int i=1; i<n_o+1; i++) {
			linee[i].SetWidth(0.1f,0.1f);
			track[i] = Vector3.Distance(posizione[i],posizione[i+1]);
			linee[i].SetPosition(0,(posizione[i+1]-posizione[i])*(oggetti[i-1].r_circ/track[i])+posizione[i]+sut);
			linee[i].SetPosition(1,(posizione[i+1]-posizione[i])*(oggetti[i-1].r_circ/track[i])+posizione[i]+sut);
		}


		cont = 0;
		counter = 0;
		stato = 0;


	
	} // FINE START
	
	//****************************************************************************** UPDATE *****
	void Update () { 
		int statoApp = stato;
		switch(stato) { //macchina a stati

		case 0: //personaggio fa riscaldamento per qualche secondo
			boy.ANIM_risc = true;
			if(firstTime) { Invoke ("zero2one", attesa_start); }
			break;

		case 1: //personaggio raggiunge prossimo ostacolo durante FASE 1
			if (cont < n_o) { //se non deve raggiungere l'arrivo
				oggetti[cont].changecolor(true);
				oggetti[cont].psyco(Time.time,2f);
				if(firstTime) { boy.Raggiungi(posizione[cont+1],v_linea,oggetti[cont].r_circ); }

				if (counter < track[cont]-oggetti[cont].r_circ) { //disegna linea solo all'esterno delle circonferenza
					counter = Vector3.Magnitude(boy.myPersonaggio.transform.position - posizione[cont]) ;
					linee [cont].SetPosition (1,boy.myPersonaggio.transform.position + sut);
				}
				if (boy.Raggiungi(posizione[cont+1],v_linea,oggetti[cont].r_circ)) { // quando è arrivato nei pressi dell'ostacolo
					counter = 0f;
					stato = 2;
				}					
			} else { //deve raggiungere l'arrivo
				if(firstTime) { boy.Raggiungi(posizione[cont+1],v_linea,0.1f); }

				if (counter < track[cont]) { //disegna linea fino in fondo
					counter = Vector3.Magnitude(boy.myPersonaggio.transform.position - posizione[cont]) ;
					linee [cont].SetPosition (1,boy.myPersonaggio.transform.position + sut);
				}
				if (boy.Raggiungi()) {//quando è arrivato all'arrivo
					stato = 5;
				}
			}
			break;
		
		case 2: //piccola pausa mentre il personaggio carica il salto durante FASE 1
			boy.ANIM_carSalto = true;
			if(firstTime) { Invoke("two2three",attesa_jump); }
			break;
		
		case 3: //personaggio salta in automatico durante FASE 1
			if(firstTime) { boy.Salta(oggetti[cont].altezza,oggetti[cont].r_circ*2f,oggetti[cont].salto); }
			if (boy.Salta ()) { //quando salto è finito
				stato = 4;
			}
			break;
		
		case 4: //personaggio fa il girotondo all'ostacolo appena saltato durante FASE 1
			if (firstTime) { boy.Girotondo(posizione[cont+1],posizione[cont+2],true); }
			if (boy.Girotondo()) { //quando girotondo è finito
				oggetti[cont].changecolor(false);
				cont = cont + 1;
				stato = 1;
			}
			break;
			
		case 5: //personaggio esulta
			boy.ANIM_fest = true;
			if (firstTime) {
				Invoke ("five2six",attesa_fase2);
				boy.Guarda(myCamera.transform.position); }
			break;

		case 6: //si aspetta che l'utente prema una cosa tipo "start"
			if (Input.GetKey(KeyCode.Alpha1)) { //pulsante premuto
				for (int i=0; i<n_o+1; i++) {
					Destroy(lineeg [i]); //distruzione linee
				}
				Destroy(boy.myPersonaggio); //distruzione personaggio
				boy = new Personaggio (posizione[0],posizione[1]); //creazione nuovo personaggio
				boy.ANIM_risc = true;
				cont = 0;
				stato = 8;
			}
			break;
			
		case 7: //inutile. by PAolo
			break;

	//FASE 2 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! FASE 2 !!!!!!!
		case 8: //personaggio segue il mouse fino al prossimo ostacolo. 
			if (Input.GetKey(KeyCode.Mouse0)) { //forza sopra prima soglia
				boy.ANIM_risc = false;
				ssx = myCamera.ScreenPointToRay(Input.mousePosition).direction.x;
				ssy = myCamera.ScreenPointToRay(Input.mousePosition).direction.y;
				ssz = myCamera.ScreenPointToRay(Input.mousePosition).direction.z;
				t = -transform.position.y/ssy;
				p = new Vector3(transform.position.x + t*ssx,0f,transform.position.z + t*ssz);

				// se l'utente ha premuto abbastanza vicino
				float distance = Vector3.Magnitude(p-boy.myPersonaggio.transform.position);
				if (distance < soglia_pick) {
					boy.Raggiungi(p,v_linea*distance,0.1f);
				} else { // l'utente ha premuto troppo lontano
					boy.Stop(); //stop
				}
			} else { // l'utente non ha premuto sullo schermo
				boy.Stop(); //stop
			}


			if (cont < n_o) { // se non deve raggiungere l'arrivo
				if (Vector3.Magnitude(posizione[cont+1]-boy.myPersonaggio.transform.position) < oggetti[cont].r_circ) {
					boy.Stop(); //stop
					boy.Girotondo(posizione[cont+1],posizione[cont],false);
					cont = cont + 1; 
					stato = 9;
				}
			} else { // se deve raggiungere l'arrivo
				if (Vector3.Magnitude(posizione[cont+1]-boy.myPersonaggio.transform.position) < 0.3f) {
					boy.Stop();
					conttel = 0;
					stato = 15;
				}
			}
			break;
		
		case 9: //personaggio si riallinea prima di saltare
			if(boy.Girotondo()) {
				max_forza = 0;
				forza = 0;
				stato = 10;
			}
			break;
		
		case 10: //aspettando che l'utente rilasci dopo aver premuto abbastanza
			boy.ANIM_carSalto = true;
			if (forza > max_forza) { //aggiorna massima forza applicata
				max_forza = forza;
			}

			if (Input.GetKeyUp(KeyCode.Mouse0)) {
				if (oggetti[cont-1].forzaLim > max_forza && max_forza > oggetti[cont-1].forza) {
					stato = 13; //forza sufficiente
					boy.ANIM_carSalto = false;
				} else {
					stato = 11; //forza troppo bassa
					boy.ANIM_carSalto = false;
				}
			}
			break;
		
		case 11: //premuto poco, personaggio salta ma non supera l'ostacolo
			if (firstTime) { boy.Salta(1f,2f,-1f); }
			max_forza = 0;
			forza = 0;
			if (boy.Salta()) {
				stato = 10; //torna ad aspettare che l'utente prema
			}
			break;
		
		case 12: //inutile. by PAolo
			break;
		
		case 13: //premuto ok, personaggio salta e supera l'ostacolo
			if (firstTime) { boy.Salta(oggetti[cont-1].altezza,oggetti[cont-1].r_circ*2f,oggetti[cont-1].salto); }
			if (boy.Salta()) {
				oggetti[cont-1].distruggi(fade_ostacoli);
				stato = 14;
			}
			break;
		
		case 14: //utile grazieeee
			if (oggetti[cont-1].vuoto == false) {
				oggetti[cont-1].distruggi(fade_ostacoli);
			}
			else {
				stato = 8;
			}
			break;
		
		case 15: // finito
			boy.ANIM_fest = true;
			
			if(firstTime) { 
				Debug.Log ("FINITO TUTTO YEEE!");
			}
			if (Vector3.Distance (myCamera.transform.position, stopc_p) > 0.001f) {
				myCamera.transform.position = new Vector3(Mathf.Lerp(startc_p.x,stopc_p.x,conttel*(0.03f/Tcam)),Mathf.Lerp(startc_p.y,stopc_p.y,conttel*(0.03f/Tcam)),Mathf.Lerp(startc_p.z,stopc_p.z,conttel*(0.03f/Tcam)));
				myCamera.transform.eulerAngles = new Vector3 (Mathf.Lerp(startc_r.x,stopc_r.x,conttel*(0.03f/Tcam)),Mathf.Lerp(startc_r.y,stopc_r.y,conttel*(0.03f/Tcam)),Mathf.Lerp(startc_r.z,stopc_r.z,conttel*(0.03f/Tcam)));
				
				boy.Guarda(Camera.main.transform.position);
				conttel += 1;
			}
			
			
			
			
			break;


		}


		if (Input.GetKeyDown (KeyCode.Alpha5)) {
			forza = 0.2f;
		}
		if (Input.GetKeyDown (KeyCode.Alpha6)) {
			forza = 0.5f;
		}
		if (Input.GetKeyDown (KeyCode.Alpha7)) {
			forza = 0.8f;
		}
		

		// gestione della forza***********************************************************************



		/* 	in "CASE  8:" sostituire "Input.GetKey(KeyCode.Mouse0)" con "actualForce>0"
			in "CASE 10:" sostituire "Input.GetKeyUp(KeyCode.Mouse0) con "actualForce<0"

		 */
	
	







		if (statoApp != stato) {
			firstTime = true;
			Debug.Log ("----------------------------------------stato: " + stato);
		} else {firstTime = false;}

	} // FINE UPDATE

	void zero2one() {
		boy.ANIM_risc = false;
		stato = 1;
		Debug.Log ("----------------------------------------stato: " + stato);
		firstTime = true;
	}
	void two2three() {
		boy.ANIM_carSalto = false;
		stato = 3;
		Debug.Log ("----------------------------------------stato: " + stato);
		firstTime = true;
	}
	void five2six() {
		boy.ANIM_fest = false;
		stato = 6;
		Debug.Log ("----------------------------------------stato: " + stato);
		firstTime = true;
	}

} // FINE CLASSE
