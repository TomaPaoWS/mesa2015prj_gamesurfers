﻿using UnityEngine;
using System.Collections;

public class Immagine : Object {
	
	public GameObject myImmagine;
	public float r_circ = 1.2f;



	public bool vuoto = false;

	static int linee = 7;
	static int campioni = 1000;
	float start;
	GameObject[] lineag = new GameObject[linee];
	LineRenderer[] linea = new LineRenderer[linee];
	int segmenti = 1000;
	Material[] matl = new Material[linee];
	static int linee_inter = 5;
	//float par_int = 0.9f;
	float angle,j,x,z,x1,z1,t; 
	float[,,] xp = new float[campioni,linee_inter,2];
	float[,,] zp = new float[campioni,linee_inter,2];
	float[] anglea = new float[campioni];
	MeshRenderer[] mrt;
	Material[] mat; 
	Material agg;
	Color aggc;
	Vector3 angolob = new Vector3 (7f, 0f, -7f);
	
	Color[] lineac = new Color[linee];
	Color[] colori;
	
	Color circ = new Color (1f, 0.973f, 0f, 0.392f);
	
	int segmentit = 1000;
	GameObject lineagt;
	LineRenderer lineat;
	float ratio;
	float spessore = 0.1f;
	float altezza_t;
	float giri;
	float T = 0.5f;
	float rtc = 5f;
	int flag;
	
	int cont;
	int flagt;
	float corona = 0.15f;
	
	float xt,zt,yt,anglet;
	
	// Inizializzatore
	public Immagine(Vector3 _posizione,string _prefab) {
		



		myImmagine = Instantiate (Resources.Load (_prefab)) as GameObject;
		myImmagine.transform.position = _posizione;
		myImmagine.transform.eulerAngles = Vector3.zero;
		myImmagine.tag ="ostacolo";
		
				
		for (int i=0; i<linee; i++) {
			lineag [i] = Instantiate (Resources.Load ("linea_circ")) as GameObject;
			lineag[i].tag = "ostacolo";
			linea [i] = lineag [i].GetComponent<LineRenderer> ();
			linea [i].SetWidth (0.05f, 0.05f);
			matl[i] = linea[i].material;
			lineac[i] = matl[i].GetColor("_TintColor");
			
			
		}
		
		linea[0].SetVertexCount(segmenti+1);
		linea[1].SetVertexCount(segmenti+1);
		
		
		
		j = 0;
		cont = 0;
		angle = 0;
		for (int i = 0; i < segmenti + 1; i++) {
			
			x = myImmagine.transform.position.x + Mathf.Sin (Mathf.Deg2Rad * angle) * r_circ;
			z = myImmagine.transform.position.z + Mathf.Cos (Mathf.Deg2Rad * angle) * r_circ;
			x1 = myImmagine.transform.position.x + Mathf.Sin (Mathf.Deg2Rad * angle) * (r_circ-corona);
			z1 = myImmagine.transform.position.z + Mathf.Cos (Mathf.Deg2Rad * angle) * (r_circ-corona);
			if (Mathf.Abs(j- angle) < (360f / segmenti) && cont < linee_inter) {
				//Debug.Log(j);
				linea[2+cont].SetPosition (0,new Vector3(x1,0.1f,z1) );
				linea[2+cont].SetPosition (1,new Vector3(x,0.1f,z) );
				
				j += (360f / linee_inter);
				cont += 1;
			}
			
			linea[0].SetPosition (i,new Vector3(x,0.1f,z) );
			linea[1].SetPosition (i,new Vector3(x1,0.1f,z1) );
			
			angle += (360f / segmenti);
			
			
			
		}
		
		
		//parabola deposita
		
		altezza_t =  1.6f * r_circ;
		giri = altezza_t / (1.2f * spessore);
		ratio = 1.2f * spessore / 360f;
		lineagt = Instantiate (Resources.Load ("lineat")) as GameObject;
		lineagt.transform.position = new Vector3(myImmagine.transform.position.x,myImmagine.transform.position.y-1.1f*altezza_t,myImmagine.transform.position.z);
		
		lineat = lineagt.GetComponent<LineRenderer> ();
		lineat.SetWidth (0.1f, 0.1f);
		lineat.useWorldSpace = false;
		lineat.SetVertexCount(segmentit+1);
		
		
		anglet = 0;
		
		for (int i = 0; i < segmentit + 1; i++) {
			
			xt =   Mathf.Sin (Mathf.Deg2Rad * anglet) * (r_circ-corona);
			zt =   Mathf.Cos (Mathf.Deg2Rad * anglet) * (r_circ-corona);
			yt =  anglet*ratio;
			
			lineat.SetPosition (i,new Vector3(xt,yt,zt) );
			
			anglet += ((giri*360f) / segmentit);
			
		}
		flagt = 0;
		
		
		
		//array seno
		angle = 0; 
		t = 0;
		for (int p = 0;p<campioni;p++) {
			for (int k = 0;k < linee_inter;k++) {
				xp[p,k,0] = myImmagine.transform.position.x + Mathf.Sin (Mathf.Deg2Rad * (angle+360f/linee_inter*k)) * (r_circ-corona);
				xp[p,k,1] = myImmagine.transform.position.x + Mathf.Sin (Mathf.Deg2Rad * (angle+360f/linee_inter*k)) * r_circ;
				zp[p,k,0] = myImmagine.transform.position.z + Mathf.Cos (Mathf.Deg2Rad * (angle+360f/linee_inter*k)) * (r_circ-corona);
				zp[p,k,1] = myImmagine.transform.position.z + Mathf.Cos (Mathf.Deg2Rad * (angle+360f/linee_inter*k)) * r_circ;
			}
			t += 2*Mathf.PI/campioni;
			angle = 90f*Mathf.Sin(t);

			anglea[p] = angle/90f*30f; // si decide l'ampiezza di rotazione
		}
		
		mrt = myImmagine.GetComponentsInChildren<MeshRenderer> ();
		mat = new Material[mrt.Length];
		colori = new Color[mrt.Length];
		for (int i = 0; i<mrt.Length; i++) {
			mat [i] = mrt [i].material;
			colori[i] = mat [i].color;
			if (mrt[i].materials.Length > 1) {
				flag = 1;
				agg = mrt[i].materials[1];
				//Debug.Log(agg.name);
				aggc = agg.color;
			}
			
			
			
		}
		
		
	}
	

	public void show(float _time,float _T) {
		int passo = (int)(((_time % _T) / _T) * campioni);
		
		myImmagine.transform.eulerAngles = new Vector3 (myImmagine.transform.eulerAngles.x, anglea [passo], myImmagine.transform.eulerAngles.z);
		
	}

	public bool scappa(float _time,float _T,Transform _ambiente,bool _start = false) {

		if (_start) {
			start = _time;
			for (int i = 0; i < linee; i++) {
				Destroy (lineag [i]);
			}
		}

		Vector3 traslazione = Vector3.Normalize (angolob - myImmagine.transform.position);
		myImmagine.transform.Translate (traslazione*(Mathf.Pow((_time - start) % _T ,3f)),_ambiente);

		

		if (Camera.main.WorldToScreenPoint(myImmagine.transform.position).x > Camera.main.pixelWidth || Camera.main.WorldToScreenPoint(myImmagine.transform.position).y < 0f) {
			Destroy (myImmagine);

			vuoto = true;
			return false;
		} else {
			return true;
		}
		
	}



	public void psyco(float _time,float _T) {
		int passo = (int)(((_time % _T) / _T) * campioni);
		
		for (int k = 0; k < linee_inter; k++) {
			linea [2 + k].SetPosition (0, new Vector3 (xp [passo, k, 0], 0.1f, zp [passo, k, 0]));
			linea [2 + k].SetPosition (1, new Vector3 (xp [passo, k, 1], 0.1f, zp [passo, k, 1]));
		}
		
	}

	public void changecolor(bool _sigira) {
		if (_sigira) {
			for (int i = 0; i < linee; i++) {
				linea [i].material.SetColor ("_TintColor", circ);
			}
		} 
		else {
			for (int i = 0; i < linee; i++) {
				linea [i].material.SetColor ("_TintColor", lineac[0]);
			}
		}
		
	}

	

		//return false;
		//		}
		
}



